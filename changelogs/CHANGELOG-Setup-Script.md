# Version 0.6.9
 - Corrected wsjt-x address from http to https
 - Added libudev-dev as fldigi dependency per merge request.

# Version 0.6.8
 - Returned Hamlib to version checking after patch for last RC.  Should again update appropriately.

# Version 0.6.7
 - Added required dependency for Electrum.
 
# Version 0.6.6
 - Changed KeepassXC on pi install to use snap installation.
 - Corrected Electrum download link.

# Version 0.6.5
 - Fixed more issues with Veracrypt.  Now installs current version properly.
 - Added dependency for Chirp

# Version 0.6.4
 - Fixed fail to compile for Direwolf
 - Fixed Keepassxc cmake failure due to dependencies.  Still not fully operational
 - Fixed Veracrypt broken link.

# Version 0.6.3
 - Hardcoded Version for Hamlib 4.0-rc2

# Version 0.6.2
 - Increased swap size to 2000 due to increasing demand by FLDIGI.

# Version 0.6.1
 - Re-enabled YAAC 
 - Fixed typos and formatting.
 - Fixed error not displaying PyBitmessage instructions
 - Added note in CLI if no failures found instead of "no such file"

# Version 0.6.0
 - Changed build dependencies for FLDIGI suite and HAMLIB.  This no longer requires enabling source code repositories.  That menu has been disabled for now and will be removed after further testing.
 - JS8Call dependencies not compatible with Ubuntu 19.10 at this time.
 - VOACAP Pythonprop GUI dependencies not compatible with Ubuntu 19.10 at this time.

# Version 0.5.9.3
 - Fixed Electrum version check / install issue
 - Changed to pulling images from local git cloned directory instead of pulling from web each run.
 - Added Bitcoin / Monero donate option to allow users to support development costs.

# Version 0.5.9.2
 - Cleaned up some formatting issues.
 - fixing conky ARDOP indication for Raspbian.

# Version 0.5.9.1
 - Updated JS8Call function to pull more accurate download links when changed.
 - Initial version checking changed for two digit version as currently listed on the website.  Will need to make more flexible.

# Version 0.5.9
 - Added Security Tools - GNU Privacy Assist

# Version 0.5.8
 - Added Security Tools - GTK Hash

# Version 0.5.7
 - Added Other Tools - VIM and Filezilla

# Version 0.5.6
 - Added recognition for Linux Mint 19.2 "Tina".  Not tested yet for dependency support.

# Version 0.5.5

 - Updated script to recognize Debian 10 for the new Raspbian OS.
 - Added dependency portaudio19-dev for build-dep fldigi requirements
 - Allowed script to recognize difference between Stretch and Buster for enabling source code.
 - Removed dead dependency for JS8Call.
 - Added Buster to the the list of supported OS's for wsjtx 
 - Added LibreOffice Suite to Other Tools
 - Fixed Error in BTC price display in Conky Large config.
 - Added recognition for Ubuntu 19.04 Disco Dingo
 - Added Raspbian / Debian Buster Electrum BTC wallet support
 - WSJTX installer not working for Ubuntu 19.04 Disco Dingo yet.  Need to tweak dependencies.

# Version 0.5.4.4

 - Updated JS8Call Script to automatically query current version (1.0 released today.)

# Version 0.5.4.3

 - Updated JS8Call Script to 1.0.0-rc3

# Version 0.5.4.1
 - Modified Conky Script to remove CPU cores.  This avoids a crash of conky if the user's system does not have as many cores as the script.
 - Adds autostart configuration for conky during final cleanup for all systems now instead of only the Pi.
 - Added directory checking to the make icons function in case it is ran by itself. 
# Version 0.5.4


 - Implemented Chainsaws Conky Script modifications
 - Hard coded the JS8Call download to the new link for version 1.0.0-rc1. I'll update the auto version checking for this once he releases downloads on the public website that I can query for up to date versions.  The current solution is no longer reliable.  Either update manually from the groups.io page or check back for updated scripts after the new versions are released.
 - Changed icons creation option to the top menu.
 
 
 
# Version 0.5.3.3

 - Made min dialog size slightly larger.
 - Changed JS8Call default to v0.14.1



 
# Version 0.5.3.2

 - Ublox GPS setup added for using the Ublox GPS for system time.


# Version 0.5.3.1


 - Temporarilly disabled YAAC install due to a dependency error.
 - Added Swap File size detection and increasing to 500 to help with the pi locking up during compiling of FLDIGI version 4.1.00 and greater.



# Version 0.5.3.0

 - Added YAAC APRS software
 - Changed ARDOP / ARDOP_GUI to be built from source on x64 systems.
 - Added missing dependency to enable FLDIGI to recognize the HamLib 3.3 installation during compiling.
 - Added an 'Install All' option the various category menus.
 - Updated default version checking of JS8Call to 0.13.0 for the newest release. 



# Version 0.5.2.1


 - Fixed small date error for eastern time on conky.
  

# Version 0.5.2.0

 - Added an ardop-status program and desktop launcher when ARDOP is selected. This opens a small program to give you the status of the ARDOP TNC, ARDOP_GUI and pat server. You can toggle them on or off for easy management of them in the background. (Still working x64 dependencies for ARDOP_GUI for Mint 18.x and Ubuntu 16.xx, current versions 19+ and 18.04+ are working.) (To add to a present install, select ARDOP and ICONS from the Radio Menu)
 - Cleaned up some error handling with desktop shortcuts.
 - Added Conky along with a few choices for conky configuration files. Autostart must be setup by the user on non-raspbian machines, .
 - Added JS8Call
 - Fixed a window sizing issue on the confirmation of selected items page and figured out scroll text.
 - Made window sizing more consistent and min size a little large for better
   readability on the pi 7" screen.
 - Added an AmRRON Corps image to the menus.
 


# Version 0.5.1.3

 - Copied grid square map to Documents folder where findardop is looking for it.


# Version 0.5.1.2

 - Fixed issue with keepassxc initial build on Pi
 
# Version 0.5.1.1

 - Fixed window size issue on failure log location menu.
 - Fixed WSJT-X dependency issue

# Version 0.5.1

 - Added WSJT-X
 - Fixed a formatting error with line wrap on the menus
 - Added some checking and reporting of most of the programs if already installed


# Version 0.5.0

 - Complete change in display program along with logic change to support YAD. Now Fully GUI rather than terminal based (still need to launch from the terminal in order to input sudo password when prompted.)
 - Implemented failure logic and logging.
 - Implemented checks for YAD installation and install if needed on start
 - Implemented checking for server status to avoid erroring out if a link goes down.
 - More formating clean up as I found the IDE formatting doesnt translate properly.
 - Wrapped some items into functions for easier disabling for testing.
 - Added pulseaudio items for install
 - Changed some of the logic to flow better.
 - Enabled program to open Sofware Sources for Ubuntu and Mint for enabling sources
 - Added VOACAP GUI
 - Added FLWRAP
 - Added ARDOP_GUI
 - Added pulseaudio suite


# Changes in 0.4.7.2

 - Fixed typo on line 333 that created --scrolltext error


# Changes in 0.4.7.1

 - Added Electrum for Linux machines (Mint 19+ and Ubuntu 18.04+ for now)
 - Added Veracrypt (user must finish install due to terminal limitations)
 - Added Hamlib 3.3 for up to date rigctl radios
 - Added FLRIG
 - Switched for all Linux machines to use ardopc instead of ardopc_64 as that is
 depricated.
 - Added RaspAP Access Point tool
 - Cleaned up some formatting (more to do)
 - Created Packet Tools location prompts
 - Fixed Paranoia Text Encrypter desktop launcher
 - Added KM4ACK Find Ardop script and Icom Grid Square Map

# Functions available in 0.4.6.2

FLDIGI FLMSG FLAMP ARDOP ARIM GARIM PAT CHIRP AmRRON-Forms Desktop-Shortcuts
MINICOM GTKTERM PUTTY DIREWOLF GEDIT On-Screen-Keyboard SCREEN SPEEDTEST
FAIL2BAN KEEPASSXC ICEWEASEL SSH-Hardening Paranoia-File-Enrypter(and stego) BITMESSAGE
Disable / Enable WiFi/Bluetooth (Pi3 only)
