## Programs from KM4ACK's Build-a-Pi Project

__Nice to have Programs__
- libreoffice
- hamclock
- propagation
- gpredict
- tqsl
- callsign
- tempconvert
- gparted
- cqrlog

__Programs to check out__
- ax25
- pat-menu
- m0iax (js8call msg)
- xastir (aprs)
- pi-aprs
- gridtracker (wsjtx grids)
- piterm (PiQTerm tcp term protocol)
- bpq (LinBPQ)
- zygrib (grib file viewer)
