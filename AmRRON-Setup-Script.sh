#!/bin/bash
################################################################################
################################################################################
########################  AmRRON Setup Script v0.6.9    ########################
########################        Created by TB-14        ########################
########################           02 Jul 21            ########################
################################################################################
################################################################################
#
#
# Sample code gathered from too many soruces to list across the internet.
# Thank you to all the people making guides and posting on StackOverflow or
# any of the other sites that I found answers to my searches.
# Specific attribution to the PiVPN project's script in which OS Detection
# and screen size modeling was used.
#
#
################################################################################
################################################################################
###############################   Instructions  ################################
################################################################################
################################################################################
# This tool will assist with the setup of a linux / raspberry pi AmRRON machine.
# It is designed to check what is installed, compare versions with the newest
# updates, and allow the user to install what they want easily.  Most packages
# are built from source to ensure you have the newest versions compiled for
# your system.
#
# To run this program it is recommended to open a terminal, resize the window
# to full screen, make the script executable and run it. You can make it
# executable by typing:
#
# chmod +x 'filename'
#
# You can run it with ./'filename'
#
# Alternatively, a 'debug' mode can be used to see what errors occur by
# running with:
#
#	bash -x filename
#
# You should also be able to run it by double clicking and selecting 'Run in terminal'
# but any errors will simply close the session and you won't know what errored or why.
#
################################################################################
################################################################################
################################################################################
###  Admin setup for the installer.  Setting window size and determining OS.####
################################################################################
################################################################################
################################################################################
# Find out if we have yad and install if not
echo "Yad is used to create the interactive menus this program uses."
echo "If yad is not installed, we will attempt to install it now."
sleep 2
if ! hash yad 2>/dev/null; then
sudo apt-get -y install yad ||
{ echo "Yad install failed.  Yad is required to run this installer.  Please install yad and try again or check your internet connection."; exit 1; }

fi

# Find the screen size. Will default to 800x480 if it can not be detected.
function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"
}



# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"

#Set image location


# Create a error function to make life easier later on
function errors() {
echo "${ERRCODE}" >> /tmp/failures
	}

# Set variables used in the script.

# Determine what is already installed.
function check_installed_programs() {

echo "Please wait while I determine what programs and versions are already present...."

if hash rigctl 2>/dev/null; then
    HAMLIB=$(rigctl --version | grep Hamlib | sed s/"rigctl(d),\ Hamlib\ "//)
        else
            HAMLIB="NOT INSTALLED"
fi


if hash fldigi 2>/dev/null; then
    FLDIGI=$(fldigi --version | awk 'FNR == 1 {print $2}')
        else
            FLDIGI="NOT INSTALLED"
fi

if hash flmsg 2>/dev/null; then
    FLMSG=$(flmsg --version | awk 'FNR == 1 {print $2}')
        else
            FLMSG="NOT INSTALLED"
fi

if hash flamp 2>/dev/null; then
    FLAMP=$(flamp --version | awk 'FNR == 1 {print $2}')
        else
            FLAMP="NOT INSTALLED"
fi

if hash flrig 2>/dev/null; then
    FLRIG=$(flrig --version | awk 'FNR == 1 {print $2}')
        else
            FLRIG="NOT INSTALLED"
fi

if hash flwrap 2>/dev/null; then
    FLWRAP=$(flwrap --version | awk 'FNR == 1 {print $2}')
        else
            FLWRAP="NOT INSTALLED"
fi

if hash pat 2>/dev/null; then
    PAT=$(pat version | awk -F 'v' '{print $2}' | head -c 5)
        else
            PAT="NOT INSTALLED"
fi

if hash chirpw 2>/dev/null; then
    CHIRP=$(chirpw --version | awk -F 'daily-' '{print $2}' | head -c 8)
        else
            CHIRP="NOT INSTALLED"
fi

if hash electrum 2>/dev/null; then
	ELECTRUM=$(electrum version)
		else
	ELECTRUM="NOT INSTALLED"
fi

if hash arim 2>/dev/null; then
    ARIM=$(arim --version | head -n1 | awk -F ' ' '{print $2}')
        else
            ARIM="NOT INSTALLED"
fi

if hash garim 2>/dev/null; then
    GARIM=$(garim --version | head -n1 | awk -F ' ' '{print $2}')
        else
            GARIM="NOT INSTALLED"
fi

if hash findardop 2>/dev/null; then
    FINDARDOPVER="INSTALLED"
        else
            FINDARDOPVER="NOT INSTALLED"
fi

if hash keepassxc 2>/dev/null; then
    KEEPASSXCVER="INSTALLED"
   else
            KEEPASSXCVER="NOT INSTALLED"
fi

if hash minicom 2>/dev/null; then
    MINICOMVER="INSTALLED"
   else
            MINICOMVER="NOT INSTALLED"
fi

if hash gtkterm 2>/dev/null; then
    GTKTERMVER="INSTALLED"
   else
            GTKTERMVER="NOT INSTALLED"
fi

if hash putty 2>/dev/null; then
    PUTTYVER="INSTALLED"
   else
            PUTTYVER="NOT INSTALLED"
fi

if hash gedit 2>/dev/null; then
    GEDITVER="INSTALLED"
   else
            GEDITVER="NOT INSTALLED"
fi

if hash screen 2>/dev/null; then
    SCREENVER="INSTALLED"
   else
            SCREENVER="NOT INSTALLED"
fi

if hash speedtest 2>/dev/null; then
    SPEEDTESTVER="INSTALLED"
   else
            SPEEDTESTVER="NOT INSTALLED"
fi

if hash matchbox-keyboard 2>/dev/null; then
    KEYBOARDVER="INSTALLED"
   else
            KEYBOARDVER="NOT INSTALLED"
fi

if hash fail2ban-client 2>/dev/null; then
    FAIL2BANVER="INSTALLED"
   else
            FAIL2BANVER="NOT INSTALLED"
fi

if hash iceweasel 2>/dev/null; then
    ICEWEASELVER="INSTALLED"
   else
            ICEWEASELVER="NOT INSTALLED"
fi

if hash pte 2>/dev/null; then
    PTEVER="INSTALLED"
   else
            PTEVER="NOT INSTALLED"
fi

if hash pfe 2>/dev/null; then
    PFEVER="INSTALLED"
   else
	    PFEVER="NOT INSTALLED"
fi

if hash wsjtx 2>/dev/null; then
    WSJTXVER="INSTALLED-v. Unknown"
else
            WSJTXVER="NOT INSTALLED"
fi

if hash conky 2>/dev/null; then
	CONKYVER="INSTALLED"
else
	CONKYVER="NOT INSTALLED"
fi

if hash js8call 2>/dev/null; then
    JS8CALLVER="INSTALLED-v. Unknown"
else
            JS8CALLVER="NOT INSTALLED"
fi

if hash yaac 2>/dev/null; then
	YAACVER="INSTALLED"
else
	YAACVER="NOT INSTALLED"
fi

if hash libreoffice 2>/dev/null; then
	LIBREVER="INSTALLED"
else
	LIBREVER="NOT INSTALLED"
fi

if hash vim 2>/dev/null; then
	VIMVER="INSTALLED"
else
	VIMVER="NOT INSTALLED"
fi

if hash filezilla 2>/dev/null; then
	FZVER="INSTALLED"
else
	FZVER="NOT INSTALLED"
fi

if hash gtkhash 2>/dev/null; then
	GTKHASHVER="INSTALLED"
else
	GTKHASHVER="NOT INSTALLED"
	
fi

if hash gpa 2>/dev/null; then
	GPAVER="INSTALLED"
else
	GPAVER="NOT INSTALLED"
fi
}


# Query what the most current versions available are.  Check that curl is
#available before starting.

function query_updates() {

echo "Checking if curl is installed..."

if ! hash curl 2>/dev/null; then
	sudo apt-get install -y curl
fi

echo "Please wait while I determine the most current versions available...."

NEWHAMLIB=$(curl -s https://sourceforge.net/projects/hamlib/files/latest/download | \
grep -o https://downloads.sourceforge.net/project/hamlib/hamlib/[0-9].[0-9] | \
head -n 1 | awk -F "/" '{print $7}')
if [[ $NEWHAMLIB == "" ]]; then
	NEWHAMLIB="SERVER DOWN"
fi


NEWFLDIGI=$(curl -s https://sourceforge.net/projects/fldigi/files/fldigi/ | \
grep .tar.gz | head -1 | awk -F "-" '{print $2}' | awk -F ".tar" '{print $1}')
if [[ $NEWFLDIGI == "" ]]; then
	NEWFLDIGI="SERVER DOWN"
fi

echo "10% Complete"

NEWFLMSG=$(curl -s https://sourceforge.net/projects/fldigi/files/flmsg/ | \
grep .tar.gz | head -1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLMSG == "" ]]; then
	NEWFLMSG="SERVER DOWN"
fi

echo "20% Complete"

NEWFLAMP=$(curl -s https://sourceforge.net/projects/fldigi/files/flamp/ | \
grep .tar.gz | head -1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLAMP == "" ]]; then
	NEWFLAMP="SERVER DOWN"
fi

echo "30% Complete"

NEWFLRIG=$(curl -s https://sourceforge.net/projects/fldigi/files/flrig/ | \
grep .tar.gz | head -1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLRIG == "" ]]; then
	NEWFLRIG="SERVER DOWN"
fi

NEWFLWRAP=$(curl -s https://sourceforge.net/projects/fldigi/files/flwrap/ | \
grep .tar.gz | head -1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLWRAP == "" ]]; then
	NEWFLWRAP="SERVER DOWN"
fi

echo "40% Complete"

NEWARIM=$(curl -s https://www.whitemesa.net/arim/arim.html | grep -m 1 \
"armv7l.tar.gz" | awk -F '-' '{print $2}')
if [[ $NEWARIM == "" ]]; then
	NEWARIM="SERVER DOWN"
fi

echo "50% Complete"

NEWGARIM=$(curl -s https://www.whitemesa.net/garim/garim.html | grep -m 1 \
"armv7l.tar.gz" | awk -F '-' '{print $2}')
if [[ $NEWGARIM == "" ]]; then
	NEWGARIM="SERVER DOWN"
fi

echo "60% Complete"

NEWPAT=$(curl -s https://github.com/la5nta/pat/releases | grep -m 1 "amd64.deb"\
 | awk -F '_' '{print $2}')
if [[ $NEWPAT == "" ]]; then
	NEWPAT="SERVER DOWN"
fi

echo "70% Complete"

NEWCHIRP=$(curl -s https://trac.chirp.danplanet.com/chirp_daily/LATEST/ | \
grep .tar.gz | awk -F 'chirp-daily-' '{print $2}' | head -c 8)
if [[ $NEWCHIRP == "" ]]; then
	NEWCHIRP="SERVER DOWN"
fi

echo "80% Complete"

NEWELECTRUM=$(curl -s https://github.com/spesmilo/electrum/blob/master/RELEASE-\
NOTES | grep Release | head -n 1 | grep -o '[0-9].[0-9].[0-9]')
if [[ $NEWELECTRUM == "" ]]; then
	NEWELECTRUM="SERVER DOWN"
fi

echo "90% Complete"

NEWVC=$(curl -s https://www.veracrypt.fr/en/Downloads.html | sed -n 's/<h3>Latest\ Stable\ Release:\ //p' | awk -F '(' '{print $1}' | sed 's/.$//')
if [[ $NEWVC == "" ]]; then
	NEWVC="SERVER DOWN"
fi

if [[ ${PLAT} = "Raspbian" ]]; then
NEWARDOP=$(curl -s http://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="piardopc"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
else
NEWARDOP=$(curl -s http://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="ardopc"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
fi

if [[ $NEWARDOP == "" ]]; then
	NEWARDOP="SERVER DOWN"
fi

if [[ ${PLAT} = "Raspbian" ]]; then
NEWARDOPGUI=$(curl -s http://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="piARDOP_GUI"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
else
NEWARDOPGUI=$(curl -s http://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="ARDOP_GUI"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
fi

if [[ $NEWARDOP == "" ]]; then
	NEWARDOP="SERVER DOWN"
fi


NEWWSJTX=$(curl -s https://physics.princeton.edu/pulsar/K1JT/wsjtx.html | grep -o "wsjtx_[0-9].[0-9].[0-9]*" | awk -F "_" '{print $2}' | head -n 1)

NEWJS8CALLVER=$(curl -s http://files.js8call.com/latest.html | grep "<strong>" | grep -o [0-9].[0-9])

echo "100% Complete"

}




##  Piping the previous function, with echo numbers, instead of percent complete, through this gets a progress bar but I lose my variables to the subshell.  Figure out alternative...
#yad --center --progress --percentage=100 --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
#--title "Getting Version Information" \
#--text "Please wait while I check on your installed versions and the newest versions available." ${r} ${c} <<<




#Deterimine computer type.
function noOS_Support() {
	yad --image=$DIR/images/corps-icon.png --center --no-buttons \
		--title "INVALID OS DETECTED" \
		--text "A valid OS was not detected.\n\nThis installer was designed for \
Debian based distros; specifically Raspbian Stretch, Linux Mint 18.3 / 19.1 and Ubuntu 18.04." \
		${r} ${c}
		exit 1

}

function maybeOS_Support() {
	if (yad --image=$DIR/images/corps-icon.png --center \
	--title "Not Supported OS" \
	--text "Your OS may be compatible with this installer but has not been \
tested.\n\nThis installer has been tested on Raspbian Stretch, Ubuntu \
18.04, and Mint 18.3 / 19.1.\n\nWould you like to continue anyway?" ${r} ${c}) then
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Did not detect perfectly supported OS but,\n\n \
::: Continuing installation at user's own risk..." ${r} ${c} ||
	    { echo "exiting"; exit 1; }
	else
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Exiting due to unsupported OS" ${r} ${c}
	    exit 1
	fi
}

# Compatibility check for OS
  # if lsb_release command is on their system
  if hash lsb_release 2>/dev/null; then

    PLAT=$(lsb_release -si)
    OSCN=$(lsb_release -sc) # We want this to be trusty xenial bionic cosmic disco; jessie strech buster; sara serena sonya sylvia tara tessa tina

  else # else get info from os-release

    PLAT=$(grep "^NAME" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"' \
    | awk '{print $1}')
    VER=$(grep "VERSION_ID" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"')
    declare -A VER_MAP=(["10"]="buster" ["9"]="stretch" ["8"]="jessie" ["16.04"]="xenial" \
    ["14.04"]="trusty" ["18.04"]="bionic" ["18.10"]="cosmic" ["19.04"]="disco" ["19.10"]="eoan" ["20.04"]="focal" ["18"]="sara" ["18.1"]="serena" \
    ["18.2"]="sonya" ["18.3"]="sylvia" ["19"]="tara" ["19.1"]="tessa" ["19.2"]="tina" ["19.3"]="tricia" ["20"]="ulyana" )
    OSCN=${VER_MAP["${VER}"]}

  fi

  case ${PLAT} in
    Ubuntu|Raspbian|Debian|LinuxMint|PureOS|Linuxmint)
      case ${OSCN} in
        trusty|xenial|bionic|cosmic|disco|eoan|focal|jessie|stretch|buster|sara|serena|sonya|sylvia|tara|tessa|tina|tricia|ulyana|amber)
          ;;
        *)
          maybeOS_Support
          ;;
      esac
      ;;
    *)
      noOS_Support
      ;;
  esac

###Determine if we have a Pi4


###
#create a finish trap
function finish {
  if [[ -f /tmp/results ]]; then
rm /tmp/results
fi

if [[ -f /tmp/mastermenu ]]; then
rm /tmp/mastermenu
fi

if [[ -f /tmp/cleanup ]]; then
rm /tmp/cleanup
fi

if [[ -f /tmp/failures ]]; then
rm /tmp/failures
fi

}
trap finish EXIT


##make sure we dont already have a cleanup, mastermenu, or results text file

if [[ -f /tmp/results ]]; then
	echo "Results file previously exists.  Moving to /tmp/results.bak"
	mv /tmp/results /tmp/results.bak
fi

if [[ -f /tmp/mastermenu ]]; then
	echo "/tmp/mastermenu file previously exists.  Moving to /tmp/mastermenu.bak"
	mv /tmp/mastermenu /tmp/mastermenu.bak
fi

if [[ -f /tmp/cleanup ]]; then
	echo "/tmp/cleanup file previously exists.  Moving to /tmp/cleanup.bak"
	mv /tmp/cleanup /tmp/cleanup.bak
fi

if [[ -f /tmp/failures ]]; then
	echo "/tmp/failures file previously exists.  Moving to /tmp/failures.bak"
	mv /tmp/failures /tmp/failures.bak
fi

################################################################################
################################################################################
####################### Radio Tools Functions ##################################
################################################################################
################################################################################

function hamlib_install() {
if [[ $NEWHAMLIB == "SERVER DOWN" ]]; then
	echo "HAMLIB server unresponsive or broken link." >> /tmp/failures
	return
fi

if [[ -d $USERDIR/hamlib-$NEWHAMLIB ]]; then
	echo "Hamlib $NEWHAMLIB already present"

else
# tcl8.5-dev libsigc++-1.2-dev, Trying libsigc++-2.0-dev tcl8.6-dev
sudo apt-get install -y cmake build-essential libusb-1.0-0.dev libltdl-dev libusb-1.0-0 libhamlib-utils libsamplerate0 libsamplerate0-dev libsigx-2.0-dev libsigc++-2.0-dev libpopt-dev tcl8.6-dev libspeex-dev libasound2-dev alsa-utils libgcrypt20-dev libpopt-dev libfltk1.3-dev libpng++-dev portaudio19-dev libpulse-dev libportaudiocpp0 libsndfile1-dev ||
{ ERRCODE="Failed to build dependencies for Hamlib"; errors; return; }

cd $USERDIR && wget -N https://sourceforge.net/projects/hamlib/files/hamlib/${NEWHAMLIB}/hamlib-${NEWHAMLIB}.tar.gz ||
 { ERRCODE="Failed to download Hamlib $NEWHAMLIB"; errors; return; }

tar -xvzf hamlib-${NEWHAMLIB}.tar.gz && rm hamlib-${NEWHAMLIB}.tar.gz && cd hamlib-${NEWHAMLIB} ||
 { ERRCODE="Failed to extract Hamlib $NEWHAMLIB"; errors; return; }


./configure && make && sudo make install ||
 { ERRCODE="Failed to compile Hamlib $NEWHAMLIB"; errors; return; }

sudo ldconfig ||
{ ERRCODE="ldconfig failed"; errors; return; }

fi


}

function fldigi_install() {
if [[ $NEWFLDIGI == "SERVER DOWN" ]]; then
	echo "FLDIGI server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLDIGI} == ${NEWFLDIGI} ]]; then
    	echo "FLDIGI is already at the most current version"

     else
          echo "Installing / Updating FLDIGI version ${NEWFLDIGI}"
		sudo apt-get install -y cmake build-essential libusb-1.0-0.dev libudev-dev libltdl-dev libusb-1.0-0 libhamlib-utils libsamplerate0 libsamplerate0-dev libsigx-2.0-dev libsigc++-2.0-dev libpopt-dev tcl8.6-dev libspeex-dev libasound2-dev alsa-utils libgcrypt20-dev libpopt-dev libfltk1.3-dev libpng++-dev portaudio19-dev libpulse-dev libportaudiocpp0 libsndfile1-dev ||
        		  { ERRCODE="Failed to get dependencies for FLDIGI."; errors; return; }

                  cd $USERDIR && wget -N https://sourceforge.net/projects/fldigi\
/files/fldigi/fldigi-${NEWFLDIGI}.tar.gz ||
                  { ERRCODE="Failed to download FLDIGI"; errors; return; }

                  tar xzvf fldigi-${NEWFLDIGI}.tar.gz && rm fldigi-${NEWFLDIGI}.\
tar.gz && cd fldigi-${NEWFLDIGI} ||
        		  { ERRCODE="Failed to extract FLDIGI"; errors; return; }

			 if [[ ${PLAT} = "Raspbian" ]]; then
            			./configure && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLDIGI"; errors; return; }
        			else
		        	  ./configure --enable-optimizations=native && make && sudo make \
install ||
{ ERRCODE="Failed to download FLDIGI"; errors; return; }
	fi
fi
}


function flmsg_install() {

if [[ $NEWFLMSG == "SERVER DOWN" ]]; then
	echo "FLMSG server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLMSG} == ${NEWFLMSG} ]]; then
            echo "FLMSG is already at the most current version"

       else
              echo "Installing / Updating FLMSG to version ${NEWFLMSG}"
            	cd $USERDIR && wget -N https://sourceforge.net/projects/fldigi/\
files/flmsg/flmsg-${NEWFLMSG}.tar.gz ||
            		  { ERRCODE="Failed to download FLMSG"; errors; return; }
            	  tar xzvf flmsg-${NEWFLMSG}.tar.gz && rm flmsg-${NEWFLMSG}.tar.gz\
                 && cd flmsg-${NEWFLMSG} ||
            		  { ERRCODE="Failed to extract FLMSG"; errors; return; }

            	  if [[ ${PLAT} = "Raspbian" ]]; then
            			./configure && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLMSG"; errors; return; }
        			else
		        	  ./configure --enable-optimizations=native && make && sudo make \
install ||
   { ERRCODE="Failed to compile FLMSG"; errors; return; }
	fi
 fi
    }


function flamp_install() {
if [[ $NEWFLAMP == "SERVER DOWN" ]]; then
	echo "FLAMP server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLAMP} == ${NEWFLAMP} ]]; then
            echo "FLAMP is already at the most current version"

         else
              echo "Installing / Updating FLAMP to version ${NEWFLAMP}"
                    sleep 2
                  cd $USERDIR && wget -N https://sourceforge.net/projects/fldigi\
/files/flamp/flamp-${NEWFLAMP}.tar.gz ||
                      { ERRCODE="Failed to download FLAMP"; errors; return; }
                  tar xzvf flamp-${NEWFLAMP}.tar.gz && \
                  rm flamp-${NEWFLAMP}.tar.gz && cd flamp-${NEWFLAMP} ||
                      { ERRCODE="Failed to extract FLAMP"; errors; return; }

                  if [[ ${PLAT} = "Raspbian" ]]; then
            			./configure && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLAMP"; errors; return; }
        			else
		        	  ./configure --enable-optimizations=native && make && sudo make \
install ||
 { ERRCODE="Failed to compile FLAMP"; errors; return; }
	fi
fi
    }


function flrig_install() {
if [[ $NEWFLRIG == "SERVER DOWN" ]]; then
	echo "FLRIG server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLRIG} == ${NEWFLRIG} ]]; then
            echo "FLRIG is already at the most current version"

         else
              echo "Installing / Updating FLRIG to version ${NEWFLRIG}"
                    sleep 2
                  cd $USERDIR && wget -N https://sourceforge.net/projects/fldigi\
/files/flrig/flrig-${NEWFLRIG}.tar.gz ||
                      { ERRCODE="Failed to download FLRIG"; errors; return; }
                  tar xzvf flrig-${NEWFLRIG}.tar.gz && rm flrig-${NEWFLRIG}\
.tar.gz && cd flrig-${NEWFLRIG} ||
                      { ERRCODE="Failed to extract FLRIG"; errors; return; }

                  if [[ ${PLAT} = "Raspbian" ]]; then
            			./configure && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLRIG"; errors; return; }
        			else
		        	  ./configure --enable-optimizations=native && make && sudo make \
install ||
{ ERRCODE="Failed to compile FLRIG"; errors; return; }
	fi
fi
    }


function flwrap_install() {
if [[ $NEWFLWRAP == "SERVER DOWN" ]]; then
	echo "NEWFLWRAP server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLWRAP} == ${NEWFLWRAP} ]]; then
            echo "FLWRAP is already at the most current version"

         else
              echo "Installing / Updating FLWRAP to version ${NEWFLWRAP}"
                    sleep 2
                  cd $USERDIR && wget -N https://sourceforge.net/projects/fldigi\
/files/flwrap/flwrap-${NEWFLWRAP}.tar.gz ||
                      { ERRCODE="Failed to download FLWRAP"; errors; return; }
                  tar xzvf flwrap-${NEWFLWRAP}.tar.gz && rm flwrap-${NEWFLWRAP}\
.tar.gz && cd flwrap-${NEWFLWRAP} ||
                      { ERRCODE="Failed to extract FLWRAP"; errors; return; }

                  if [[ ${PLAT} = "Raspbian" ]]; then
            			./configure && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLWRAP"; errors; return; }
        			else
		        	  ./configure --enable-optimizations=native && make && sudo make \
install ||
{ ERRCODE="Failed to compile FLWRAP"; errors; return; }
	fi
fi
    }



function ardop_install() {

		ARDOPDIR=$USERDIR/ARDOP

		if [[ ${PLAT} = "Raspbian" ]]; then
			ARDOP=$"piardopc"

			if [[ -f $ARDOPDIR/$ARDOP ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOP $ARDOPDIR/${ARDOP}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop file"; errors; return; }
                    	fi

			echo "Downloading current $ARDOP version."
                    	wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/$ARDOP && chmod +x $ARDOPDIR/$ARDOP ||
                    		{ ERRCODE="Failed to download ardop"; errors; return; }


		else
			ARDOP=$"ardopc"


			if [[ -f $ARDOPDIR/$ARDOP ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOP $ARDOPDIR/${ARDOP}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop file"; errors; return; }
                    	fi

                sudo apt-get -y install libasound2-dev ||
		{ ERRCODE="Failed to get dependencies for ARDOPC"; errors; return; }

            		echo "Downloading current $ARDOP version."
                    	wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/TeensyProjects.zip && unzip -d $ARDOPDIR/ $ARDOPDIR/TeensyProjects.zip && rm $ARDOPDIR/TeensyProjects.zip ||
                    		{ ERRCODE="Failed to download and extract ardop source"; errors; return; }

			cd $ARDOPDIR/TeensyProjects/ARDOPC/ && make ||
			{ ERRCODE="Failed to compile ardop source code"; errors; return; }

			mv $ARDOPDIR/TeensyProjects/ARDOPC/ardopc $USERDIR/ARDOP/ ||
			{ ERRCODE="Failed to move ardopc to $ARDOPDIR"; errors; return; }

			rm -r $ARDOPDIR/TeensyProjects/ ||
			{ ERRCODE="Failed to remove source code directory"; errors; return; }
fi

	if [[ ! -f $HOME/.asoundrc ]]; then
		  echo "pcm.ARDOP {
			type rate
			slave {
			pcm "\""hw:1,0"\""
			rate 48000
			}
			}" > $HOME/.asoundrc ||
       			{ ERRCODE="Failed to create .asoundrc configuration file"; errors; return; }
	fi
  	echo ardop >> /tmp/cleanup
	echo asoundrc >> /tmp/cleanup

   if ! hash ardop-status 2>/dev/null; then
    ardop_launcher
    fi


      }


function ardop_gui_install() {

		ARDOPDIR=$USERDIR/ARDOP

		if [[ ${PLAT} = "Raspbian" ]]; then
			ARDOPGUI=$"piARDOP_GUI"

	  		if [[ -f $ARDOPDIR/$ARDOPGUI ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOPGUI $ARDOPDIR/${ARDOPGUI}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop_gui file"; errors; return; }
                    	fi

			echo "Downloading current $ARDOPGUI version."
                    	wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/$ARDOPGUI && chmod +x $ARDOPDIR/$ARDOPGUI ||
                    		{ ERRCODE="Failed to download ardop_gui"; errors; return; }




else

		ARDOPGUI=$"ARDOP_GUI"

			if [[ -f $ARDOPDIR/$ARDOPGUI ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOPGUI $ARDOPDIR/${ARDOPGUI}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop_gui file"; errors; return; }
                    	fi

               sudo apt-get -y install g++ qt5-default ||
		{ ERRCODE="Failed to get dependencies for ARDOP_GUI"; errors; return; }

               wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/ARDOP_GUI.zip && unzip -d $ARDOPDIR/ardop_gui-source $ARDOPDIR/ARDOP_GUI.zip && rm $ARDOPDIR/ARDOP_GUI.zip ||
                    		{ ERRCODE="Failed to download  and extract ardop_gui source code"; errors; return; }
		cd $ARDOPDIR/ardop_gui-source && qmake && make ||
			{ ERRCODE="Failed to compile ardop_gui source code"; errors; return; }

		mv $ARDOPDIR/ardop_gui-source/ARDOP_GUI $USERDIR/ARDOP/ ||
			{ ERRCODE="Failed to move ardop_gui to $ARDOPDIR"; errors; return; }

		rm -r $ARDOPDIR/ardop_gui-source/ ||
			{ ERRCODE="Failed to remove ARDOP_GUI source code directory"; errors; return; }

fi





      }

function ardop_launcher() {

if [[ ${PLAT} = "Raspbian" ]]; then
	local var1="piardopc"
	local var13="piARDOP_GUI"
	else
	local var1="ardopc"
	local var13="ARDOP_GUI"
fi
local var2='$(pidof '"$var1"')'
local var3='$(pidof pat)'
local var14='$(pidof '"$var13"')'
local var4='$ardopstat'
local var5='$ardoppid'
local var6='$patstat'
local var7='$patpid'
local var15='$guistat'
local var16='$guipid'
local var8='$(cat /tmp/process-status | head -n 1 | awk -F "|" '"'"'{print $1}'"'"')'
local var9='$(cat /tmp/process-status | sed -n '"'"2p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var17='$(cat /tmp/process-status | sed -n '"'"3p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var10='${ARDOPRESULTS}'
local var11='${PATRESULTS}'
local var18='${GUIRESULTS}'
local var12=$USERDIR



cat > $USERDIR/ardop-status <<EOF

#!/bin/bash



#create a finish trap
function finish {
  if [[ -f /tmp/process-status ]]; then
rm /tmp/process-status
fi

}
trap finish EXIT


while true
do



if pidof $var1 2>/dev/null; then
ardopstat="TRUE"
ardoppid=$var2
else
ardopstat="FALSE"
ardoppid="None"
fi

if pidof $var13 2>/dev/null; then
guistat="TRUE"
guipid=$var14
else
guistat="FALSE"
guipid="None"
fi

if pidof pat 2>/dev/null; then
patstat="TRUE"
patpid=$var3
else
patstat="FALSE"
patpid="None"
fi

if ! hash pat 2>/dev/null; then
patstat="NOT_INSTALLED"
patpid="NOT_INSTALLED"
fi

yad --center --checklist --list --print-all \
--title=Status \
--button=Close:1 --button=Toggle:0 \
--column=Active --column=Name --column=PID  \
$var4 ARDOPC $var5 \
$var6 PAT $var7 \
$var15 ARDOP_GUI $var16 \
--width=250 --height=200 >> /tmp/process-status || exit 0



ARDOPRESULTS=$var8
PATRESULTS=$var9
GUIRESULTS=$var17

if ( [[ $var4 == TRUE ]] && [[ $var10 == FALSE ]] ); then
kill $var5
fi

if ( [[ $var4 == FALSE ]] && [[ $var10 == TRUE ]] ); then
$var12/ARDOP/$var1 &
fi

if ( [[ $var6 == TRUE ]] && [[ $var11 == FALSE ]] ); then
kill $var7
fi

if ( [[ $var6 == FALSE ]] && [[ $var11 == TRUE ]] ); then
pat http &
fi

if ( [[ $var15 == TRUE ]] && [[ $var18 == FALSE ]] ); then
kill $var16
fi

if ( [[ $var15 == FALSE ]] && [[ $var18 == TRUE ]] ); then
$var12/ARDOP/$var13 &
fi


finish
sleep 2

done

EOF

chmod +x $USERDIR/ardop-status
sudo mv $USERDIR/ardop-status /usr/local/bin/ardop-status

}


function arim_install() {
if [[ $NEWARIM == "SERVER DOWN" ]]; then
	echo "ARIM server unresponsive or broken link." >> /tmp/failures
	return
fi
if [[ ${ARIM} == ${NEWARIM} ]]; then
            echo "ARIM is already at the most current version"

         else
              echo "Installing / Updating ARIM to version ${NEWARIM}"
			if [[ ${ARIM} == "NOT INSTALLED" ]]; then
			sudo apt-get -y install build-essential ncurses-dev zlib1g-dev ||
			{ ERRCODE="Failed to get dependencies for ARIM"; errors; return; }
			fi
		cd $USERDIR && wget -N https://www.whitemesa.net/arim/src/\
arim-${NEWARIM}.tar.gz ||
	  	{ ERRCODE="Failed to download ARIM"; errors; return; }

		tar xzvf arim-${NEWARIM}.tar.gz && rm arim-${NEWARIM}.tar.gz && \
cd arim-${NEWARIM} ||
 	 	{ ERRCODE="Failed to extract ARIM"; errors; return; }

		./configure && make && sudo make install ||
  		{ ERRCODE="Failed to compile ARIM"; errors; return; }
	fi
   }


function garim_install() {
if [[ $NEWGARIM == "SERVER DOWN" ]]; then
	echo "GARIM server unresponsive or broken link." >> /tmp/failures
	return
fi

if [[ ${GARIM} == ${NEWGARIM} ]]; then
            echo "GARIM is already at the most current version"

         else
              echo "Installing / Updating GARIM to version ${NEWGARIM}"
			if [[ ${GARIM} == "NOT INSTALLED" ]]; then
			sudo apt-get -y install build-essential libfltk1.3-dev zlib1g-dev ||
			{ ERRCODE="Failed to get dependencies for GARIM"; errors; return; }
			fi
		cd $USERDIR && wget -N https://www.whitemesa.net/garim/src/\
garim-${NEWGARIM}.tar.gz ||
	  	{ ERRCODE="Failed to download GARIM"; errors; return; }

		tar xzvf garim-${NEWGARIM}.tar.gz && rm garim-${NEWGARIM}.tar.gz && \
cd garim-${NEWGARIM} ||
 	 	{ ERRCODE="Failed to extract GARIM"; errors; return; }

		./configure && make && sudo make install ||
  		{ ERRCODE="Failed to compile GARIM"; errors; return; }
	fi
   }



function pat_install() {
if [[ $NEWPAT == "SERVER DOWN" ]]; then
	echo "PAT server unresponsive or broken link." >> /tmp/failures
	return
fi
		if [[ ${PLAT} = "Raspbian" ]]; then
			SYS=$"armhf"
		elif [[ $(uname -m) = x86_64 ]]; then
			SYS=$"amd64"
		else
			SYS=$"i386"
		fi


		if [[ ${PAT} == ${NEWPAT} ]]; then
           		echo "PAT is already at the most current version"

        	 else
			cd $USERDIR && wget -N https://github.com/la5nta/pat/releases/download\
/v$NEWPAT/pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to download PAT"; errors; return; }

			sudo dpkg -i pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to install PAT"; errors; return; }

			rm pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to remove PAT deb file."; errors; return; }
		fi
}

function getardoptools_install() {
  # Get Grid Square Map
  if ! hash evince; then
  sudo apt-get install -y evince
fi
  if [[ ! -f $USERDIR/2013_GridSquareMap.pdf ]]; then
  wget -P $USERDIR -N http://www.icomamerica.com/en/amateur/amateurtools/2013_\
GridSquareMap.pdf && cp $USERDIR/2013_GridSquareMap.pdf $HOME/Documents/grid-map.pdf ||
{ ERRCODE="Failed to download ICOM Grid Square Map"; errors; return; }
fi
  # Get ardop list script

if ! hash getardoplist; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
{ ERRCODE="Failed to download getardoplist script"; errors; return; }

  chmod +x $USERDIR/getardoplist && sudo mv $USERDIR/getardoplist /usr/local/bin/ ||
{ ERRCODE="Failed to add getardoplist to /usr/local/bin"; errors; return; }
fi
  # Get find ardop script
if ! hash findardop; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/7af44d08d95215d\
a45be3e69decf6def/raw/1aeff0d78f85e86b868a4ba83ebce99e90532f48/findardop ||
{ ERRCODE="Failed to download findardop script"; errors; return; }

    chmod +x $USERDIR/findardop && sudo mv $USERDIR/findardop /usr/local/bin/ ||
{ ERRCODE="Failed to add findardop to /usr/local/bin"; errors; return; }
fi

  echo ardoptools >> /tmp/cleanup

}


function pulse_audio_suite_install() {

sudo apt-get -y install pulseaudio pavucontrol pulseaudio-utils ||
{ ERRCODE="Failed to install pulseaudio suite"; errors; return; }

}

function voacap_install() {

	sudo apt-get -y install gfortran ||
                { ERRCODE="Failed to get gfortran dependency for VOACAPL"; errors; return; }

	cd $USERDIR && wget -N www.qsl.net/hz1jw/voacapl/downloads/voacapl-0.7.2.tar.gz ||
                { ERRCODE="Failed to download VOACAPL"; errors; return; }

	tar -xzf voacapl-0.7.2.tar.gz && rm voacapl-0.7.2.tar.gz && cd voacapl-0.7.2 ||
                { ERRCODE="Failed to extract VOACAPL"; errors; return; }


	./configure && make && sudo make install ||
                { ERRCODE="Failed to compile VOACAPL"; errors; return; }


	makeitshfbc ||
                { ERRCODE="VOACAPL makeitshfbc failed"; errors; return; }

	if [[ $OSCN =~ (eoan|focal|amber|ulyana) ]]; then
		{ echo voacap_os >> /tmp/cleanup; return; }
	  else		

	sudo apt-get -y install yelp python3-gi python3-gi-cairo rarian-compat gnome-doc-utils \
pkg-config python3-dateutil python3-mpltoolkits.basemap python3-cairocffi libgtk-3-dev gettext ||
                { ERRCODE="Failed to get dependencies for VOACAPL pythonprop GUI"; errors; return; }

	cd $USERDIR && wget -N www.qsl.net/hz1jw/pythonprop/downloads/pythonprop-0.28.tar.gz ||
                { ERRCODE="Failed to download VOACAPL pythonprop GUI"; errors; return; }


	tar -xzf pythonprop-0.28.tar.gz && rm pythonprop-0.28.tar.gz && cd pythonprop-0.28 ||
		{ ERRCODE="Failed to extract VOACAPL pythonprop GUI"; errors; return; }


	./configure && sudo make install ||
		{ ERRCODE="Failed to compile VOACAPL pythonprop GUI"; errors; return; }
fi
}

function chirp_install() {
if [[ $NEWCHIRP == "SERVER DOWN" ]]; then
	echo "CHIRP server unresponsive or broken link." >> /tmp/failures
	return
fi

if [[ ${CHIRP} == ${NEWCHIRP} ]]; then
            echo "Chirp is already at the most current version"


         else
              echo "Installing / Updating CHIRP to version ${NEWCHIRP}"
                    sleep 2
		  if [[ ${CHIRP} == "NOT INSTALLED" ]]; then
			sudo apt-get install -y python-gtk2 python-serial python-libxml2 python-future ||
			{ ERRCODE="Failed to get dependencies for CHIRP"; errors; return; }
			fi
                  cd $USERDIR && wget -N https://trac.chirp.danplanet.com/\
chirp_daily/LATEST/chirp-daily-${NEWCHIRP}.tar.gz ||
                      { ERRCODE="Failed to download CHIRP"; errors; return; }
                  tar xzvf chirp-daily-${NEWCHIRP}.tar.gz && rm chirp-daily-\
${NEWCHIRP}.tar.gz ||
                      { ERRCODE="Failed to extract CHIRP"; errors; return; }

		cd $USERDIR/chirp-daily-${NEWCHIRP} ||
			{ ERRCODE="Failed to enter CHIRP directory"; errors; return; }

		sudo python setup.py install ||
			{ ERRCODE="Failed to install CHIRP"; errors; return; }
            fi

	sudo usermod -aG dialout $USER ||
	{ ERRCODE="Failed to add $USER to dialout group"; errors; return; }


}

function wsjtx_install() {

if [[ $NEWWSJTX == "SERVER DOWN" ]]; then
	echo "WSJTX server unresponsive or broken link." >> /tmp/failures
	return
fi
		if [[ ${PLAT} = "Raspbian" ]]; then
			SYS=$"armhf"
		elif [[ $(uname -m) = x86_64 ]]; then
			SYS=$"amd64"
		else
			SYS=$"i386"
		fi


		if [[ ${WSJTXVER} == ${NEWWSJTX} ]]; then
           		echo "WSJTX is already at the most current version"

        	 else

			if [[ $OSCN =~ (tara|tessa|tina|tricia|bionic|eoan|focal|cosmic|stretch|buster|ulyana|amber) ]]; then

			echo "Getting dependencies"
        		sudo apt-get -y install libqt5multimedia5-plugins libqt5serialport5 libqt5printsupport5 \
			libqt5sql5-sqlite libfftw3-single3 libgfortran3 ||
			{ ERRCODE="Failed to get dependencies for WSJTX"; errors; return; }


			cd $USERDIR && wget -N https://physics.princeton.edu/pulsar/K1JT/wsjtx_${NEWWSJTX}_${SYS}.deb ||
			{ ERRCODE="Failed to download WSJTX"; errors; return; }

			sudo dpkg -i wsjtx_${NEWWSJTX}_${SYS}.deb ||
			{ ERRCODE="Failed to install WSJTX"; errors; return; }

			rm wsjtx_${NEWWSJTX}_${SYS}.deb ||
			{ ERRCODE="Failed to remove WSJTX deb file."; errors; return; }


			else

			echo wsjtxunsupported >> /tmp/cleanup

			fi
		fi

}

function amrronforms_install() {
     FORMSDIR=$HOME/.nbems/CUSTOM
     if [ ! -d $HOME/.nbems/CUSTOM ]; then
   mkdir -p $HOME/.nbems/CUSTOM/ ||
     { ERRCODE="Failed to create directory at $HOME/.nbems/CUSTOM/"; errors; return; }
      fi

     if ls $FORMSDIR/*REPV3.html 1>/dev/null 2>&1; then
     echo "V3 of Forms are already installed"
        else
         cd $FORMSDIR && wget -N https://amrron.com/wp-content/uploads/2014/\
11/AmRRON-Custom-Forms-V3.zip ||
       { ERRCODE="Failed to download AmRRON Custom Forms V3"; errors; return; }
        fi

        if [[ -f $FORMSDIR/AMRRON_BLANKV3.html ]]; then
            echo "AmRRON Blank V3 is already installed."
        else
            cd $FORMSDIR && wget -N https://amrron.com/wp-content/uploads/\
2013/11/AMRRON_BLANKV3.zip ||
       { ERRCODE="Failed to download AmRRON Blank V3"; errors; return; }
        fi

        if [[ -f $FORMSDIR/AmRRON_StatRepv2.html ]]; then
            echo "AmRRON STATREP V2 is already installed"
        else
            cd $FORMSDIR && wget -N https://amrron.com/wp-content/uploads/\
2018/03/AmRRON_StatRepv2.zip ||
       { ERRCODE="Failed to download AmRRON StatRep V2"; errors; return; }
        fi

        if ls $FORMSDIR/*zip 1>/dev/null 2>&1; then
        echo "Extracting forms..."
           unzip -o $FORMSDIR/'*'.zip && rm $FORMSDIR/*.zip ||
       { ERRCODE="Failed to move forms to $HOME/.nbems/CUSTOM/"; errors; return; }
            else
                echo "All custom forms are already installed."
            fi
 }


function js8call_install() {
#if [[ ${PLAT} = "Raspbian" ]]; then
#			SYS=$"armhf"
#		elif [[ $(uname -m) = x86_64 ]]; then
#			 if [[ $OSCN =~ (tara|tessa|tina|bionic|cosmic|disco) ]]; then
#			SYS=$"18.04_amd64"
#			 elif [[ $OSCN =~ (sara|serena|sonya|sylvia|trusty|xenial) ]]; then
#			SYS=$"16.04_amd64"
#			fi
#		else
#			SYS=$"i386"
#		fi

if [[ ${PLAT} = "Raspbian" ]]; then
			js8call_link=$(curl -s http://files.js8call.com/latest.html | grep -o "http://files.js8call.com/[0-9].[0-9].[0-9]/js8call_[0-9].[0-9].[0-9]_armhf.deb" | head -n 1)
		elif [[ $(uname -m) = x86_64 ]]; then
			 if [[ $OSCN =~ (tara|tessa|tina|tricia|bionic|cosmic|disco) ]]; then
			js8call_link=$(curl -s http://files.js8call.com/latest.html | grep -o "http://files.js8call.com/[0-9].[0-9].[0-9]/js8call_[0-9].[0-9].[0-9]_18.04_amd64.deb" | head -n 1)
			 elif [[ $OSCN =~ (sara|serena|sonya|sylvia|trusty|xenial) ]]; then
			js8call_link=$(curl -s http://files.js8call.com/latest.html | grep -o "http://files.js8call.com/[0-9].[0-9].[0-9]/js8call_[0-9].[0-9].[0-9]_16.04_amd64.deb" | head -n 1)
			 elif [[ $OSCN =~ (eoan|focal|amber|ulyana) ]]; then
			js8call_link=$(curl -s http://files.js8call.com/latest.html | grep -o "http://files.js8call.com/[0-9].[0-9].[0-9]/js8call_[0-9].[0-9].[0-9]_20.04_amd64.deb" | head -n 1)
			fi
		else
			js8call_link=$(curl -s http://files.js8call.com/latest.html | grep -o "http://files.js8call.com/[0-9].[0-9].[0-9]/js8call_[0-9].[0-9].[0-9]_i386.deb" | head -n 1)
		fi



sudo apt-get -y install libqt5multimedia5 libqt5multimedia5-plugins libqt5multimediawidgets5 libqt5serialport5 ||
       { ERRCODE="Failed to get dependencies for JS8Call"; errors; return; }
#removed libqgsttools-p1 from dependencies

#wget -P $USERDIR/ -N http://files.js8call.com/${NEWJS8CALLVER}/js8call_${NEWJS8CALLVER}_$SYS.deb ||
#       { ERRCODE="Failed to download JS8Call"; errors; return; }
wget -P $USERDIR/ -N ${js8call_link} ||
       { ERRCODE="Failed to download JS8Call"; errors; return; }

js8call_deb=$(echo ${js8call_link} | awk -F "/" '{print $5}')
sudo dpkg -i $USERDIR/$js8call_deb ||
       { ERRCODE="Failed to install JS8Call"; errors; return; }



}

function ublox_gps_setup() {

sudo apt-get -y install gpsd gpsd-clients python-gps chrony ||
       { ERRCODE="Failed to install gpsd gpsd-clients python-gps chrony packages"; errors; return; }

if [[ -f /etc/default/gpsd ]]; then

local var1=$(cat /etc/default/gpsd | grep START_DAEMON= | awk -F "=" '{print $2}' | sed s/\"//g)

local var2=$(cat /etc/default/gpsd | grep USBAUTO= | awk -F "=" '{print $2}' | sed s/\"//g)

local var3=$(cat /etc/default/gpsd | grep DEVICES= | awk -F "=" '{print $2}' | sed s/\"//g)

local var4=$(cat /etc/default/gpsd | grep GPSD_OPTIONS= | awk -F "=" '{print $2}' | sed s/\"//g)

if [[ $var1 != "true" ]]; then

sudo sed -i s/START_DAEMON=\"$var1\"/START_DAEMON=\"true\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set START_DAEMON to true in /etc/default/gpsd"; errors; return; }

fi


if [[ $var2 != "false" ]]; then

sudo sed -i s/USBAUTO=\"$var2\"/USBAUTO=\"false\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set USBAUTO to false in /etc/default/gpsd"; errors; return; }

fi

if [[ $var3 != "/dev/ttyACM0" ]]; then

sudo sed -i s@DEVICES=\"$var3\"@DEVICES=\"/dev/ttyACM0\"@g /etc/default/gpsd ||
       { ERRCODE="Failed to set DEVICES to /dev/ttyACM0 in /etc/default/gpsd"; errors; return; }

fi

if [[ $var4 != "-n" ]]; then

sudo sed -i s/GPSD_OPTIONS=\"$var4\"/GPSD_OPTIONS=\"-n\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set GPSD_OPTIONS to -n in /etc/default/gpsd"; errors; return; }

fi

fi

if [[ -f /etc/chrony/chrony.conf ]]; then

if grep -q "refclock SHM 0 offset 0.0 delay 0.2 refid NMEA" /etc/chrony/chrony.conf; then
	echo "Parameters already set"
	else
	echo 'refclock SHM 0 offset 0.0 delay 0.2 refid NMEA' | sudo tee -a /etc/chrony/chrony.conf ||
       { ERRCODE="Failed add: refclock SHM 0 offset 0.0 delay 0.2 refid NMEA to /etc/chrony/chrony.conf"; errors; return; }
fi
fi

}

function create_icons() {

function assign_directories() {

UNSETDIRS=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="|" \
--title "Set Installed Directories" \
--field="Tip:TXT" "One or more of the Directory Variables have not been set.  Likely due to running install icons by itself.

Please inform the installer of the following Directories so it can make the requested desktop icons.

The default locations are prefilled.

If you did not install any programs for the associated section, you can leave the selection at default." \
--field="Radio Directory:MDIR" "$HOME/Radio" \
--field="Packet Directory:MDIR" "$HOME/Radio/Packet" \
--field="Security Directory:MDIR" "$HOME/Security" \
${r} ${c})

USERDIR=$(echo $UNSETDIRS | awk -F "|" '{print $2}')
PACKETDIR=$(echo $UNSETDIRS | awk -F "|" '{print $3}')
SECDIR=$(echo $UNSETDIRS | awk -F "|" '{print $4}')

}


if ( [[ $USERDIR == "" ]] || [[ $SECDIR == "" ]] || [[ $PACKETDIR == "" ]] ); then
assign_directories
fi



if hash fldigi 2>/dev/null; then
    echo "[Desktop Entry]
    Name=Fldigi
    GenericName=Amateur Radio Digital Modem
    Comment=Amateur Radio Sound Card Communications
    Exec=fldigi
    Icon=fldigi
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/fldigi.desktop ||
       { ERRCODE="Failed to create desktop icon for FLDIGI"; errors; }
fi

if hash flmsg 2>/dev/null; then
       echo "[Desktop Entry]
       Name=Flmsg
       GenericName=Amateur Radio Digital Modem
       Comment=Amateur Radio Sound Card Communications
       Exec=flmsg
       Icon=flmsg
       Terminal=false
       Type=Application
       Categories=Network;HamRadio;" > $HOME/Desktop/flmsg.desktop ||
          { ERRCODE="Failed to create desktop icon for FLMSG"; errors; }
fi

if hash flamp 2>/dev/null; then
          echo "[Desktop Entry]
          Name=Flamp
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flamp
          Icon=flamp
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flamp.desktop ||
            { ERRCODE="Failed to create desktop icon for FLAMP"; errors; }
fi

if hash flrig 2>/dev/null; then
          echo "[Desktop Entry]
          Name=Flrig
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flrig
          Icon=flrig
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flrig.desktop ||
            { ERRCODE="Failed to create desktop icon for FLRIG"; errors; }
fi

if hash flwrap 2>/dev/null; then
          echo "[Desktop Entry]
          Name=FLWRAP
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flwrap
          Icon=flwrap
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flwrap.desktop ||
            { ERRCODE="Failed to create desktop icon for flwrap"; errors; }
fi

if hash voacapgui 2>/dev/null; then

echo "[Desktop Entry]
Name=VOACAPGUI
GenericName=VOACAP Graphical User Interface
Comment=VOACAP Propagation Tool
Exec=voacapgui
Icon=$USERDIR/icons/voacaplIcon.png
Terminal=false
Type=Application
Categories=Network;HamRadio;" > $HOME/Desktop/voacapgui.desktop ||
{ ERRCODE="Failed to create desktop icon for VOACAP GUI"; errors; }

	if [[ ! -f $USERDIR/icons/voacaplIcon.png ]]; then
	mkdir -p $USERDIR/icons && \
wget -P $USERDIR/icons/ -N https://www.qsl.net/hz1jw/voacapl/downloads/voacaplIcon.png ||
	{ ERRCODE="Failed to icon image for VOACAPGUI"; errors; }
	fi
fi

if hash ardop-status 2>/dev/null; then

echo "[Desktop Entry]
Name=Status
GenericName=ARDOP-Status
Comment=Ardop & PAT status tool
Exec=bash -c "ardop-status"
Icon=$USERDIR/icons/tnc_icon.png
Terminal=false
Type=Application
Categories=Network;HamRadio;" > $HOME/Desktop/ardop-status.desktop ||
{ ERRCODE="Failed to create desktop icon for ardop-status tool"; errors; }

	if [[ ! -f $USERDIR/icons/tnc_icon.png ]]; then
	mkdir -p $USERDIR/icons && \
wget -P $USERDIR/icons/ -N http://www.gencat.cat/llengua/appdelasetmana/img_app/2016_03_24_TNC/tnc_icon.png ||
	{ ERRCODE="Failed to icon image for ardop-status tool"; errors; }
	fi
fi


if [[ -f $USERDIR/garim-$NEWGARIM/garim.desktop ]]; then
	cp $USERDIR/garim-$NEWGARIM/garim.desktop $HOME/Desktop/garim.desktop ||
	{ ERRCODE="Failed to create desktop icon for GARIM"; errors; }
fi


if [[ -f $SECDIR/Paranoia\ Text\ Encryption/pte.jar ]]; then

echo "[Desktop Entry]
Name=PTE
GenericName=Paranoia Text Encrypter
Comment=Text encrypter
Exec=pte
Icon=$USERDIR/icons/pte_icon.png
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/PTE.desktop ||
{ ERRCODE="Failed to create desktop icon for PTE"; errors; }

if [[ ! -f $USERDIR/icons/pte_icon.png ]]; then
	mkdir -p $USERDIR/icons && wget --no-check-certificate -P $USERDIR/icons/ -N https://www.paranoia\
works.mobi/ptepc/img/pte_icon.png ||
	{ ERRCODE="Failed to icon image for PTE"; errors; }
	fi
fi

if [[ -f $SECDIR/SSEFilePC/ssefencgui.jar ]]; then

echo "[Desktop Entry]
Name=PFE
GenericName=Paranoia File Encrypter
Comment=File encrypter
Exec=pfe
Icon=$USERDIR/icons/pfe_icon.png
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/PFE.desktop ||
{ ERRCODE="Failed to create desktop icon for PFE"; errors; }

if [[ ! -f $USERDIR/icons/pfe_icon.png ]]; then
	mkdir -p $USERDIR/icons && wget --no-check-certificate -P $USERDIR/icons/ -N https://www.paranoia\
works.mobi/ssefepc/img/pfe_icon.png ||
	{ ERRCODE="Failed to icon image for PFE"; errors; }
	fi
fi

if hash keepassxc; then

echo "[Desktop Entry]
Name=KeepassXC
GenericName=KeepassXC
Comment=Password Manager
Exec=keepassxc
Icon=keepassxc
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/keepassxc.desktop ||
{ ERRCODE="Failed to create desktop icon for KeepassXC"; errors; }

fi

if hash gtkterm 2>/dev/null; then
    echo "[Desktop Entry]
    Name=GtkTerm
    GenericName=Serial Terminal Application
    Comment=Serial Terminal Application
    Exec=gtkterm
    Icon=$USERDIR/icons/gtkterm-icon.png
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/gtkterm.desktop ||
       { ERRCODE="Failed to create desktop icon for GTKTerm"; errors; }

	if [[ ! -f $USERDIR/icons/gtkterm-icon.png ]]; then
	mkdir -p $USERDIR/icons && wget -N http://icons.iconarchive.com/icons/\
papirus-team/papirus-apps/256/gtkterm-icon.png && mv gtkterm-icon.png \
  $USERDIR/icons/ ||
	{ ERRCODE="Failed to download icon image for GTKTerm"; errors; }
	fi

fi


if hash putty 2>/dev/null; then
    echo "[Desktop Entry]
    Name=PUTTY
    GenericName=Serial Terminal Application
    Comment=Serial Terminal Application
    Exec=putty
    Icon=putty
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/putty.desktop ||
       { ERRCODE="Failed to create desktop icon for Putty"; errors; }

fi

if hash pybitmessage 2>/dev/null; then
echo "[Desktop Entry]
Name=PyBitmessage
GenericName=Bitmessage
Comment=Secure Email
Exec=pybitmessage
Icon=pybitmessage
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/pybitmessage.desktop ||
	{ ERRCODE="Failed to create desktop icon for Bitmessage"; errors; }
fi

if hash veracrypt 2>/dev/null; then
echo "[Desktop Entry]
Name=Veracrypt
GenericName=Veracrypt
Comment=Text encrypter
Exec=veracrypt
Icon=veracrypt
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/Veracrypt.desktop ||
	{ ERRCODE="Failed to create desktop icon for Veracrypt"; errors; }
fi

if hash electrum 2>/dev/null; then
echo "[Desktop Entry]
Name=electrum
GenericName=BTC Wallet
Comment=Electrum Bitcoin Wallet
Exec=electrum
Icon=electrum
Terminal=false
Type=Application
Categories=Cryptocurrency;" > $HOME/Desktop/electrum.desktop ||
	{ ERRCODE="Failed to create desktop icon for Electrum"; errors; }
fi

if hash wsjtx 2>/dev/null; then
echo "[Desktop Entry]
Version=1.0
Name=wsjtx
Comment=Amateur Radio Weak Signal Operating
Exec=wsjtx
Icon=wsjtx_icon
Terminal=false
X-MultipleArgs=false
Type=Application
Categories=AudioVideo;Audio;HamRadio;
StartupNotify=true;" > $HOME/Desktop/wsjtx.desktop ||
	{ ERRCODE="Failed to create desktop icon for WSJTX"; errors;  }
fi

if hash js8call 2>/dev/null; then
    echo "[Desktop Entry]
    Name=JS8Call
    GenericName=JS8Call
    Comment=Amateur Radio Weak Signal mode
    Exec=js8call
    Icon=js8call_icon
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/js8call.desktop ||
       { ERRCODE="Failed to create desktop icon for JS8Call"; errors; }
fi

if hash direwolf 2>/dev/null; then
  if [[ ${PLAT} = "Raspbian" ]]; then
    cd $PACKETDIR/direwolf && make install-rpi ||
	{ ERRCODE="Failed to create desktop icon for Direwolf"; errors;  }
  fi
fi

if hash yaac 2>/dev/null; then
  echo "[Desktop Entry]
    Name=YAAC
    GenericName=Yet Another APRS Client
    Comment=YAAC
    Exec=yaac
    Icon=$USERDIR/icons/yaaclogo.png
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/yaac.desktop ||
       { ERRCODE="Failed to create desktop icon for YAAC"; errors; }

	if [[ ! -f $USERDIR/icons/yaaclogo.png ]]; then
	mkdir -p $USERDIR/icons && wget -P $USERDIR/icons/ -N  www.ka2ddo.org/ka2ddo/yaaclogo.png ||
	{ ERRCODE="Failed to download icon image for YAAC"; errors; }
	fi

fi

chmod +x $HOME/Desktop/*.desktop

}
################################################################################
######################   Packet tools Functions ################################
################################################################################

function minicom_install() {
sudo apt-get -y install minicom
}


function gtkterm_install() {
sudo apt-get -y install gtkterm
}

function putty_install() {
sudo apt-get -y install putty
}


function direwolf_install() {

  if [[ -d $PACKETDIR/direwolf ]]; then
    cd $PACKETDIR/direwolf && git pull ||
	{ ERRCODE="Failed to update Direwolf"; errors; return; }
  else
  sudo apt-get install -y libasound2-dev ax25* libc6-dev ||
	{ ERRCODE="Failed to get dependencies for Direwolf"; errors; return; }

  cd $PACKETDIR && git clone https://www.github.com/wb2osz/direwolf ||
  	{ ERRCODE="Failed to download Direwolf"; errors; return; }

  mkdir $PACKETDIR/direwolf/build && cd $PACKETDIR/direwolf/build && cmake .. && make -j4 && sudo make install && make install-conf ||
  	{ ERRCODE="Failed to compile Direwolf"; errors; return; }

  fi

}

function yaac_install() {

wget -P $PACKETDIR -N https://www.ka2ddo.org/ka2ddo/YAAC.zip ||
  	{ ERRCODE="Failed to download YAAC"; errors; return; }

sudo apt-get -y install ca-certificates-java ||
  	{ ERRCODE="Failed to get ca-certificates-java dependencies for YAAC"; errors; return; }

sudo apt-get -y install openjdk-8-jre librxtx-java unzip ||
  	{ ERRCODE="Failed to get openjdk-8-jre librxtx-java unzip dependencies for YAAC"; errors; return; }

unzip -o -d $PACKETDIR/YAAC $PACKETDIR/YAAC.zip ||
  	{ ERRCODE="Failed to unzip YAAC"; errors; return; }

if ! hash yaac; then

cat > $PACKETDIR/yaac <<EOF
#!/bin/bash

java -jar $PACKETDIR/YAAC/YAAC.jar

EOF

chmod +x $PACKETDIR/yaac && sudo mv $PACKETDIR/yaac /usr/local/bin/yaac ||
  	{ ERRCODE="Failed to create launcher for YAAC in /usr/local/bin/"; errors; return; }

fi

}

function packet_guide() {
cat > $HOME/Documents/Packet-Radio-setup.txt <<EOF

You should have a few tools installed for use with packet radio modes.

If you have a non-KISS TNC (i.e. Kantronics KPC3+ etc) use minicom to talk \
to your TNC.
Minicom will need to run as root. You can specify the device with a -d argument.

i.e. sudo minicom -d /dev/ttyUSB0

Or

sudo minicom -s

This will start with the setup prompt where you can select your device and \
then set it as default. Set speed to 9600.

You should then be able to access your TNC with just

sudo minicom

I found using the RT Systems Kantronics to USB cable it didnt want to work \
properly.

type lsusb -v

write down your product and vendor id for the cable.

Next,

sudo chmod 666 /sys/bus/usb-serial/drivers/ftdi_sio/new_id

Finally, add your product and vendor id into the file.

nano /sys/bus/usb-serial/drivers/ftdi_sio/new_id

For example the only text in mine is
2100 004


If you do not have a TNC.

direwolf is a software TNC which you can use for packet. I recommend reading \
up online or watching some youtube videos but here are the basics.

Type:

nano direwolf.conf

Set audio device, usually hw1,0 only need to uncomment. Change Callsign

ctrl-x with y to save the filename.

now run direwolf with a -p flag and itll create a new port for the TNC.

direwolf -p

Now we will open a new terminal.

From here we want to change our ax25 ports.
sudo nano /etc/ax25/axports

Follow the example listing
1	MYCALL-1	1200	255	2	144.930 MHz (1200 bps)

1 is the name of this port. You can change that to what ever works for you.

Now we will attach the TNC to the AX25 stack

sudo kissattach /tmp/kisstnc 1

We can use axlisten or axcall.

If you want to connect to W7ABC-8 you can type

axcall 1 w7abc-8

I've noticed that my first call doesnt seem to go out properly every time.

If I kill it with ctrl-c and then try again it seems to work.

We can release the TNC by killing kissattach

sudo killall kissattach


This should get you started playing with packet.

If you are using direwolf, your soundcard to radio cable is important.
I've had good luck with the EasyDigi VOX from ebay. A real kantronics works \
much better for me so far. A basic KISS TNC is handled just like direwolf \
(sudo kissattach DEVICE PORT and axcall to send)
Most KISS TNC's have very limited buffers and are intended for use with \
APRS, not full packet.

Search youtube for some great videos on setting up direwolf.

EOF
}
################################################################################
######################   Other tools Functions #################################
################################################################################
function vim_install() {
sudo apt-get -y install vim
}

function filezilla_install() {
sudo apt-get -y install filezilla
}

function gedit_install() {
sudo apt-get -y install gedit
}

function libreoffice_install() {
sudo apt -y install libreoffice
}

function keyboard_install() {
sudo apt-get -y install matchbox-keyboard
}

function screen_install() {
sudo apt-get -y install screen
}

function speedtest_install() {
sudo apt-get -y install speedtest-cli
}

function raspap_install() {

  if [[ ${PLAT} = "Raspbian" ]]; then
  wget -q https://git.io/voEUQ -O $HOME/Desktop/raspap ||
    { ERRCODE="Failed to download RaspAP script"; errors; return; }

  cat > $HOME/Documents/RaspAP-Hotspot-Setup.txt <<EOF
  RaspAP from https://github.com/billz/raspap-webgui#quick-installer

  After the reboot at the end of the installation the wireless network will \
  be configured as an access point as follows:

    IP address: 10.3.141.1
    Username: admin
    Password: secret
    DHCP range: 10.3.141.50 to 10.3.141.255
    SSID: raspi-webgui
    Password: ChangeMe


EOF

echo raspapsetup >> /tmp/cleanup

else

  echo noraspapsetup >> /tmp/cleanup

fi

}


function electrum_install() {
if [[ $NEWELECTRUM == "SERVER DOWN" ]]; then
	echo "Electrum server unresponsive or broken link." >> /tmp/failures
	return
fi
  if [[ ${ELECTRUM} == ${NEWELECTRUM} ]]; then
        echo "Electrum is already at the Version $NEWELECTRUM"
    else

      if [[ $OSCN =~ (tara|tessa|tina|tricia|bionic|cosmic|disco|eoan|focal|buster|amber|ulyana) ]]; then
        echo "Getting dependencies"
        sudo apt-get install -y python3-setuptools python3-pyqt5 python3-pip libsecp256k1-dev ||
	{ ERRCODE="Failed to get dependencies for Electrum"; errors; return; }

        sudo python3 -m pip install https://download.electrum.org/$NEWELECTRUM/\
Electrum-${NEWELECTRUM}.tar.gz ||
        { ERRCODE="Failed to install Electrum"; errors; return; }
      else
        echo noelectrum >> /tmp/cleanup
      fi
  fi
}

function conky_install() {

if ! hash conky; then
sudo apt-get -y install conky-all ||
        { ERRCODE="Failed to install Conky"; errors; return; }
fi

if [[ -f $HOME/.conkyrc ]]; then
mv $HOME/.conkyrc $HOME/.conkyrc-$(date +%h%d).bak ||
        { ERRCODE="Failed to backup old .conkyrc file"; errors; return; }
fi

### Move Conky.Config before size case statement since so much is shared --CS

	cat > $HOME/.conkyrc <<\EOF
conky.config = {
    alignment = 'top_right',
    background = false,
    border_width = 0,
	border_inner_margin = 15,
    color1 = '19A094',
    color2 = 'FF5252',
    cpu_avg_samples = 2,
	default_color = 'grey',
    default_outline_color = 'grey',
    default_shade_color = 'grey',
	double_buffer = true,
	draw_borders = true,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Monaco:size=12',
    gap_x = 20,
    gap_y = 50,
    minimum_height = 5,
    maximum_width = 400,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    own_window_argb_visual = true,
    own_window_argb_value = 0,
    stippled_borders = 0,
    update_interval = 1.2,
    uppercase = none,
    use_spacer = 'left',
    show_graph_scale = false,
    show_graph_range = false
}
EOF

case $CONKYSEL in
	LARGE)
    # Use Conky.Config as is, append conky.text
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=22}\
Pacific:${goto 175}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=22}${tztime America/Los_Angeles %H:%M:%S}\
${goto 175}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Los_Angeles %A %m-%d-%y}\
${goto 175}${tztime America/New_York %A %m-%d-%y}\
${alignr}${tztime Zulu %A %m-%d-%y}

$alignc ${font Roboto:style=Medium:pixelsize=35} ${color grey}\
${color D4AF37} BTC: ${execi 60 echo -n "$" ; curl -s https://api.coinbase.com/v2/prices/spot?currency=USD | grep -o [0-9][0-9][0-9][0-9][0-9].[0-9][0-9]}
#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 240}\
$cpu% ${goto 280}${cpubar cpu0 10,140}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 240}$memperc% ${goto 280}${membar 10,140}
${color2}Swap:${color grey} $swap/$swapmax\
 ${color1}${goto 240}$swapperc% ${goto 280}${swapbar 10,140}
${color2}Temp: ${color grey}${acpitemp}°C
${color2}/ ${color grey}\
 ${goto 90}${fs_used /}/${fs_size /}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /}% ${fs_bar 10,120 /}
${color2}/home ${color grey}\
 ${goto 90}${fs_used /home}/${fs_size /home}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /home}% ${fs_bar 10,120 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 270}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 200}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total:${totaldown wlp1s0} \
 ${goto 210}${color}Total:${totalup wlp1s0}
${downspeedgraph wlp1s0 30,195 00ffff 19A094} \
${upspeedgraph wlp1s0 30,195 00ffff 19A094}
${endif}\
${if_up tun0}\
${color}${font}\
${color2}tun0\
 ${color}${font}${goto 270}${addr tun0}
 ${color}Total:${totaldown tun0} \
 ${goto 210}${color}Total:${totalup tun0}
${downspeedgraph tun0 30,195 00ffff 19A094} \
${upspeedgraph tun0 30,195 00ffff 19A094}${endif}
${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${alignr}${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
PROCESSES$color$font $hr
Total:$processes  Running:$running_processes
${color2} Name             ${goto 205}PID         ${goto 275}CPU%     ${goto 350}MEM%
${color} ${top name 1} ${goto 200}${top pid 1} ${goto 275}${top cpu 1} ${goto 350}${top mem 1}
${color} ${top name 2} ${goto 200}${top pid 2} ${goto 275}${top cpu 2} ${goto 350}${top mem 2}
${color} ${top name 3} ${goto 200}${top pid 3} ${goto 275}${top cpu 3} ${goto 350}${top mem 3}
${color} ${top name 4} ${goto 200}${top pid 4} ${goto 275}${top cpu 4} ${goto 350}${top mem 4}
${color} ${top name 5} ${goto 200}${top pid 5} ${goto 275}${top cpu 5} ${goto 350}${top mem 5}

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	STANDARD)
    ### Edit ConkyConfig Large for us
    sed -i 's/Monaco:size=12/Monaco:size=10/' $HOME/.conkyrc
    sed -i 's/maximum_width = 400/maximum_width = 300/'  $HOME/.conkyrc

###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=14}\
Pacific:${goto 135}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=12}${tztime America/Los_Angeles %H:%M:%S}\
${goto 135}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Los_Angeles %m-%d-%y}\
${goto 135}${tztime America/New_York %m-%d-%y}\
${alignr}${tztime Zulu %m-%d-%y}

#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 180}\
$cpu% ${goto 210}${cpubar cpu0 10,105}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 180}$memperc% ${goto 210}${membar 10,105}
${color2}Temp: ${color grey}${acpitemp}°C


${color2}/ ${color grey}\
 ${goto 65}${fs_used /}/${fs_size /}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /}% ${goto 210}${fs_bar 10,105 /}
${color2}/home ${color grey}\
 ${goto 65}${fs_used /home}/${fs_size /home}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /home}% ${goto 210}${fs_bar 10,105 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 230}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 155}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total Down:${totaldown wlp1s0} \
 ${goto 200}${color}Total Up:${totalup wlp1s0}
${endif}
${if_up tun0}\
${color}${font}\
${color2}VPN tun0\
 ${color}${font}${goto 230}${addr tun0}
 ${color}Total Down:${totaldown tun0} \
 ${goto 200}${color}Total Up:${totalup tun0}${endif}

${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	MINIMAL)
	### Edit ConkyConfig Large
    sed -i 's/own_window_argb_visual = true/own_window_argb_visual = false/'  $HOME/.conkyrc

    ###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF

conky.text = [[

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 wget -q -O - "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/ "|grep -m1 "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5" -o | head -n1}

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP INACTIVE${endif}
${if_running pat http}${color green}PAT ACTIVE${else}${color red}PAT INACTIVE${endif}
]]
EOF
	;;
esac ||
        { ERRCODE="Failed to create .conkyrc configuration file."; errors; return; }

if [[ ${PLAT} = "Raspbian" ]]; then
sed -i '/own_window_argb_value/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_value from .conkyrc for raspbian stability"; errors; return; }

sed -i '/own_window_argb_visual/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_visual from .conkyrc for raspbian stability"; errors; return; }

sed -i s/'desktop'/'normal'/ $HOME/.conkyrc ||
        { ERRCODE="Failed to change own_window_type to normal for raspbian stability."; errors; return; }

sed -i s/'ardopc'/'piardopc'/ $HOME/.conkyrc ||
        { ERRCODE="Failed to change ardopc to piardop for raspbian status."; errors; return; }
fi

#### Find ethernet and wifi interfaces and update conkyrc --CS
## default ethernet = eth0  default wifi = wlp1s0
##  use 2nd field from ip link and word starts with e for all ethernet, w for all wireless
##  Only picks first one of each.
export ethernet_interface=$(ip link | cut -d: -f2 | grep  '\be')
export wifi_interface=$(ip link | cut -d: -f2 | grep  '\bw')

### Test to make sure the string isnt blank (doesnt have that type of interface)
[ $ethernet_interface ] && ( sed -i "s/eth0/$ethernet_interface/g" $HOME/.conkyrc )
[ $wifi_interface ]     && ( sed -i "s/wlp1s0/$wifi_interface/g" $HOME/.conkyrc  )


echo conkysetup >> /tmp/cleanup

}

################################################################################
######################   Security tools Functions ##############################
################################################################################
function fail2ban_install() {
sudo apt-get -y install fail2ban
}

function keepassxc_install() {
#if [[ ${PLAT} = "Raspbian" ]]; then

#if hash keepassxc 2>/dev/null; then

#		cd $SECDIR/keepassxc/ && git checkout master && git pull ||
#		{ ERRCODE="Failed to update KeepassXC"; errors; return; }
#	else
#
#		sudo apt-get install -y make cmake qtbase5-dev libyubikey-dev \
#    libykpers-1-dev libargon2-0-dev libgcrypt20 zlib1g libmicrohttpd-dev \
#    libxi-dev libxtst-dev qtx11extras5-doc libsodium-dev qttools5-dev \
#    libqrencode-dev libqt5svg5-dev asciidoctor libreadline-dev libqt5x11extras5-dev \ 
#    libquazip-dev qtbase5-private-dev ||
#		{ ERRCODE="Failed to get dependencies for KeepassXC"; errors; return; }
#
#		git clone https://github.com/keepassxreboot/keepassxc.git $SECDIR/keepassxc ||
#		{ ERRCODE="Failed to download KeepassXC"; errors; return; }
#
#		cd $SECDIR/keepassxc && git checkout master ||
#		{ ERRCODE="Failed to check out master branch of KeepassXC"; errors; return; }
#
#		mkdir -p $SECDIR/keepassxc/build && cd $SECDIR/keepassxc/build ||
#		{ ERRCODE="Failed to create and enter KeepassXC build directory"; errors; return; }
#
#		cmake -DWITH_XC_ALL=ON -DCMAKE_BUILD_TYPE=Release .. ||
#		{ ERRCODE="cmake failed to compile KeepassXC"; errors; return; }
#
#		make -j8 ||
#		{ ERRCODE="Failed to make KeepassXC"; errors; return; }
#
#		sudo make install ||
#		{ ERRCODE="Failed to install KeepassXC"; errors; return; }
#fi
#else
if hash snap 2>/dev/null; then
sudo snap install core && sudo snap install keepassxc ||
{ ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

else

sudo apt-get install -y snapd && sudo snap install core && sudo snap install keepassxc ||
{ ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

fi
#fi
}


function iceweasel_install() {
sudo apt-get install -y iceweasel
}

function gtkhash_install() {
sudo apt-get install -y gtkhash
}

function gpa_install() {
sudo apt-get install -y gpa
}

function harden_ssh() {
if [[ ! -f $HOME/.ssh/authorized_keys ]]; then
	mkdir -p $HOME/.ssh/ && touch $HOME/.ssh/authorized_keys ||
		{ ERRCODE="Failed to create SSH folders"; errors; return; }

	chmod 700 $HOME/.ssh/ ||
		{ ERRCODE="Failed to modify permission on $HOME/.ssh/"; errors; return; }

	chmod 600 $HOME/.ssh/authorized_keys ||
		{ ERRCODE="Failed to modify permission on $HOME/.ssh/authorized_keys"; errors; return; }


fi


cat > $HOME/Documents/SSH-setup.txt <<EOF
In order to setup secure ssh using preshared keys from your computer to the \
pi do the following:

From your computer: If you do not already have a public/private key, \
generate one;

ssh-keygen -b 4096 -t rsa

copy the public key from ~/.ssh/id_rsa.pub to your raspberry pi authorized_keys\
 file in /home/pi/.ssh/authorized_keys

You can do this by entering:
cat ~/.ssh/id_rsa.pub | ssh pi@raspberrypi.local 'cat >> .ssh/authorized_keys'

Substitute your ip address of the pi in the preceding command if \
raspberrypi.local doesn't work for you.

Now try to ssh into the pi (exmaple: ssh pi@192.168.1.2)

If it works with out a password, you're set.

Now disable access from anyone without the key.

sudo nano /etc/ssh/sshd_config

uncomment (remove the '#') and change yes to no on the line that says \
'#PasswordAuthentication yes'

I also recommend changing the default port in this file from 22 to something \
much higher. If you do this, you will then need to specify the port you \
chose when you connect. i.e., ssh pi@192.168.1.2 -p 1776

If you want to make an alias for ssh into your pi from your computer do \
the following.

nano .ssh/config

This will open or create a file called config in your .ssh folder.

Add the following information, adjusting as required / wanted. Again, you can \
replace raspberrypi.local with a specific IP address if needed.

Host radio
	HostName raspberrypi.local
	User pi
	Port XXX

Now we should be able to just type;
ssh radio

EOF

}


function pte_install() {

if ! hash java; then
sudo apt-get install -y default-jre ||
{ ERRCODE="Failed to get default-jre dependency for PFE"; errors; return; }
fi

cd $SECDIR && wget -N http://www.paranoiaworks.mobi/download/files/PTE-PC.zip ||
	{ ERRCODE="Failed to download PTE"; errors; return; }

unzip -o PTE-PC.zip && rm PTE-PC.zip ||
	{ ERRCODE="Failed to unzip PTE"; errors; return; }

if ! hash pte 2>/dev/null; then

cat > /tmp/pte <<EOF
#!/bin/bash
java -jar '$SECDIR/Paranoia Text Encryption/pte.jar'
EOF

chmod +x /tmp/pte && sudo mv /tmp/pte /usr/local/bin/pte ||
{ ERRCODE="Failed to create system launcher for PTE in /usr/local/bin/"; errors; return; }

fi
}


function pfe_install() {

if ! hash java; then
sudo apt-get install -y default-jre ||
{ ERRCODE="Failed to get default-jre dependency for PFE"; errors; return; }
fi

cd $SECDIR && wget -N http://www.paranoiaworks.mobi/download/files/SSEFilePC.zip ||
	{ ERRCODE="Failed to download PFE"; errors; return; }

unzip -o SSEFilePC.zip && rm SSEFilePC.zip ||
	{ ERRCODE="Failed to unzip PFE"; errors; return; }

if ! hash pfe 2>/dev/null; then

cat > /tmp/pfe <<EOF
#!/bin/bash
java -jar '$SECDIR/SSEFilePC/ssefencgui.jar'
EOF

chmod +x /tmp/pfe && sudo mv /tmp/pfe /usr/local/bin/pfe ||
{ ERRCODE="Failed to create system launcher for PFE in /usr/local/bin/"; errors; return; }

fi
}

function veracrypt_install() {
if [[ $NEWVC == "SERVER DOWN" ]]; then
	echo "Veracrypt server unresponsive or broken link." >> /tmp/failures
	return
fi
NEWVC_lower=$(echo $NEWVC | tr '[:upper:]' '[:lower:]')

  if [[ ${PLAT} = "Raspbian" ]]; then
    
    VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC_lower}/+download/veracrypt-${NEWVC}-Debian-10-armhf.deb"
    
  elif [[ $OSCN =~ (eoan|focal|amber|ulyana) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC_lower}/+download/veracrypt-${NEWVC}-Ubuntu-19.10-amd64.deb"

  elif [[ $OSCN =~ (disco) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC_lower}/+download/veracrypt-${NEWVC}-Ubuntu-19.04-amd64.deb"

  elif [[ $OSCN =~ (bionic|cosmic|tara|tessa|tina|tricia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC_lower}/+download/veracrypt-${NEWVC}-Ubuntu-18.04-amd64.deb"

  elif [[ $OSCN =~ (xenial|yakkety|zesty|artful|sarah|serena|sonya|sylvia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC_lower}/+download/veracrypt-${NEWVC}-Ubuntu-16.04-amd64.deb"
      


  fi

#if [[ ${PLAT} = "Raspbian" ]]; then
#  VCFILE=$(echo ${VCLINK} | awk -F '/' '{print $9}')
#	else
VCFILE=$(echo ${VCLINK} | awk -F '/' '{print $8}')
#fi

  VCFILE_name=$(echo ${VCFILE} | awk -F '.tar.bz2' '{print $1}')


    cd $SECDIR && wget -N ${VCLINK} ||
      { ERRCODE="Failed to download Veracrypt"; errors; return; }

#	if [[ ${PLAT} = "Raspbian" ]]; then

#	tar xvf ${VCFILE} && rm ${VCFILE} ||
#        { ERRCODE="Failed to extract Veracrypt"; errors; return; }

 #       rm *-console-arm ||
#	{ ERRCODE="Failed to remove extra CLI setup files for Veracrypt"; errors; return; }

 #       echo vc >> /tmp/cleanup
      
#	else

	sudo apt-get install -y libwxgtk3.0-gtk3-0v5 libwxbase3.0-0v5 ||
	{ ERRCODE="Failed to get dependencies for Veracrypt"; errors; return; }

        sudo dpkg -i ${VCFILE} ||
	{ ERRCODE="Failed to install Veracrypt"; errors; return; }
 #  fi

}

function bitmessage_install() {
if hash pybitmessage 2>/dev/null; then

		cd $SECDIR/PyBitmessage/src && git fetch --all && git reset --hard \
origin/master ||
    { ERRCODE="Failed to update Bitmessage"; errors; return; }

	else
sudo apt-get install -y python openssl libssl-dev python-msgpack python-qt4 \
python-setuptools python-pyopencl g++ ||
	{ ERRCODE="Failed to get dependencies for Bitmessage"; errors; return; }

cd $SECDIR/ && git clone https://github.com/Bitmessage/PyBitmessage ||
	{ ERRCODE="Failed to download Bitmessage"; errors; return; }

cd $SECDIR/PyBitmessage && sudo python setup.py install ||
	{ ERRCODE="Failed to install Bitmessage"; errors; return; }
fi

echo pybitmessagesetup >> /tmp/cleanup
}


function wifi_off() {
if [[ ${PLAT} = "Raspbian" ]]; then
	if grep -q dtoverlay=pi3-disable-wifi /boot/config.txt; then
		echo "Wifi is already disabled at boot"
	else
		echo "dtoverlay=pi3-disable-wifi" | sudo tee -a /boot/config.txt

cat > $HOME/Documents/wifi-bt-disabled.txt <<EOF
Wifi and/or Bluetooth have been disabled from loading at boot.

Should you wish to re-enable these functions you can run the script, \
selecting WIFI_ON or BT_ON or you will need to manually edit your \
/boot/config.txt file.

The bottom two lines will have:
dtoverlay=pi3-disable-bt
dtoverlay=pi3-disable-wifi

You can either comment these out (place a # before the line) or delete \
these lines.  Save your changes and reboot the Pi.

If you want to use the script to disable them in the future please delete \
the lines rather than commenting them out.

EOF

	echo wirelessoff >> /tmp/cleanup

	fi
else
	echo "Wifi Disable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi

}

function bt_off() {
if [[ ${PLAT} = "Raspbian" ]]; then
	if grep -q dtoverlay=pi3-disable-bt /boot/config.txt; then
		echo "Bluetooth is already disabled at boot"
	else
		echo "dtoverlay=pi3-disable-bt" | sudo tee -a /boot/config.txt
cat > $HOME/Documents/wifi-bt-disabled.txt <<EOF
Wifi and/or Bluetooth have been disabled from loading at boot.

Should you wish to re-enable these functions you can run the script, selecting \
WIFI_ON or BT_ON or you will need to manually edit your /boot/config.txt file.

The bottom two lines will have:
dtoverlay=pi3-disable-bt
dtoverlay=pi3-disable-wifi

You can either comment these out (place a # before the line) or delete these \
lines.  Save your changes and reboot the Pi.

If you want to use the script to disable them in the future please delete the \
lines rather than commenting them out.

EOF

	echo wirelessoff >> /tmp/cleanup

	fi
else
	echo "Bluetooth Disable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi

}

function wifi_on() {
if [[ ${PLAT} = "Raspbian" ]]; then
	if grep -q dtoverlay=pi3-disable-wifi /boot/config.txt; then
		sudo sed -i s/dtoverlay=pi3-disable-wifi//g /boot/config.txt
	else
		echo wirelesson >> /tmp/cleanup
	fi
else
	echo "WIFI Enable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi
}

function bt_on() {
if [[ ${PLAT} = "Raspbian" ]]; then
	if grep -q dtoverlay=pi3-disable-bt /boot/config.txt; then
		sudo sed -i s/dtoverlay=pi3-disable-bt//g /boot/config.txt
	else
		echo wirelesson >> /tmp/cleanup
	fi
else
	echo "Bluetooth Enable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi
}


################################################################################
###############################   Menus   ######################################
################################################################################

#### Ham Radio tools menu ###


function radio_installer() {

	#Set user install directory For Radio Tools.
	UserDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Radio Tools Installer / Updater" \
	--text "Select your desired installation folder for HAM Radio Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Radio" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${UserDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Radio Tools Directory" \
	      --text "You have selected $HOME/Radio as your installation directory" ${r} ${c} ||
	      { radio_installer; return; }

	        USERDIR=$HOME/Radio
	        if [[ ! -d $USERDIR ]]; then
	        mkdir -p $USERDIR
	        fi
	        ;;
	    Custom)
	        USERDIR=$(yad --center --form --separator="" --field="MDIR:MDIR" $HOME \
		--image=$DIR/images/corps-icon.png \
	        --title "Radio Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { radio_installer; return; }
	        if [[ ! -d $USERDIR ]]; then
	        mkdir -p $USERDIR
	        fi
	        ;;
	    esac



yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Radio Programs" \
--text "Select the programs you would like to install"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Program" --column="Description" \
--column="Installed version" --column="Updated Version" \
 false "HAMLIB" "Hamlib libraries provide rig control for your radio" \
 "${HAMLIB}" "${NEWHAMLIB}" \
 false "FLDIGI" "FLDIGI Radio Program" "${FLDIGI}" "${NEWFLDIGI}" \
 false "FLMSG" "FLMSG Radio Program" "${FLMSG}" "${NEWFLMSG}" \
 false "FLAMP" "FLAMP Radio Program" "${FLAMP}" "${NEWFLAMP}" \
 false "FLRIG" "FLRIG Radio Control Program" "${FLRIG}" "${NEWFLRIG}" \
 false "FLWRAP" "FLWRAP Radio Control Program" "${FLWRAP}" "${NEWFLWRAP}" \
 false "ARDOP" "Downloads the newest ARDOP v1 TNC" "Unknown" "${NEWARDOP}" \
 false "ARDOPGUI" "Downloads the newest ARDOP Graphical User Interface" "Unknown" "${NEWARDOPGUI}" \
 false "ARIM" "Amateur Radio Instant Messanger" "${ARIM}" "${NEWARIM}" \
 false "GARIM" "Graphical ARIM interface" "${GARIM}" "${NEWGARIM}" \
 false "PAT" "ARDOP email client / Winlink alternative" "${PAT}"  "${NEWPAT}" \
 false "FINDARDOP" "Program by KM4ACK to find ardop stations" "${FINDARDOPVER}" "Unable to determine" \
 false "VOACAP" "VOACAP Propagation tool and python GUI." "Unable to determine" "Unable to determine" \
 false "CHIRP" "Radio Programing Software" "${CHIRP}" "${NEWCHIRP}" \
 false "WSJTX" "WSJTX Weak Signal QSO program" "${WSJTXVER}" "${NEWWSJTX}" \
 false "JS8CALL" "JS8Call Weak Signal QSO program." "${JS8CALLVER}" "${NEWJS8CALLVER}" \
 false "PULSE" "Pulseaudio suite.  Includes pulseaudio, pavucontrol, and pulseaudio-utils." "Unable to determine" "Unable to determine" \
 false "UBLOX" "Installs programs and settings to use the UBLOX GPS for system time." "Unable to determine" "Unable to determine" \
 false "FORMS" "AmRRON Form templates will be downloaded." "Unknown" "Version 3" \
 ${r} ${c} >>/tmp/results

rc=$?
 if [[ $rc == 2 ]]; then
  radioArray=(HAMLIB FLDIGI FLMSG FLAMP FLRIG FLWRAP ARDOP ARDOPGUI ARIM GARIM PAT FINDARDOP VOACAP CHIRP WSJTX JS8CALL PULSE UBLOX FORMS)

	for i in "${radioArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}

### Packet Radio tools ###

function packet_installer() {

	#Set user install directory For Packet Tools.
	PacketDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Packet Tools Installer / Updater" \
	--text "Select your desired installation folder for Packet Radio Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Radio/Packet" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${PacketDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Packet Tools Directory" \
	      --text "You have selected $HOME/Radio/Packet as your installation directory" ${r} ${c} ||
	      { packet_installer; return; }

	        PACKETDIR=$HOME/Radio/Packet
	        if [[ ! -d $PACKETDIR ]]; then
	        mkdir -p $PACKETDIR
	        fi
	        ;;
	    Custom)
	        PACKETDIR=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="" --field="MDIR:MDIR" $HOME \
	        --title "Packet Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { packet_installer; return; }
	        if [[ ! -d $PACKETDIR ]]; then
	        mkdir -p $PACKETDIR
	        fi
	        ;;
	    esac


yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Packet Programs" \
--text "Select the packet programs you would like to install"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "MINICOM" "Basic command line serial terminal.  Useful for SSH access." "${MINICOMVER}" \
 false "GTKTERM" "GTK based terminal - Recommended" "${GTKTERMVER}" \
 false "PUTTY" "Popular graphical terminal" "${PUTTYVER}" \
 false "DIREWOLF" "Soundcard TNC" "" \
 false "YAAC" "Yet Another APRS Client" "${YAACVER}" \
 false "PACKET_GUIDE" "Creates a guide in $HOME/Documents with basic information to get you started with packet operations" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  packetArray=(MINICOM GTKTERM PUTTY DIREWOLF YAAC PACKET_GUIDE)

	for i in "${packetArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}

#### Other tools menu ###

function other_installer(){
yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Other Programs" \
--text "Select the other programs you would like to install. RASPAP not included in 'Install All'"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "GEDIT" "Graphical Text Editor." "${GEDITVER}" \
 false "VIM" "VIM text editor." "${VIMVER}" \
 false "FILEZILLA" "Graphical FTP program" "${FZVER}" \
 false "MATCHBOX_KEYBOARD" "Touchscreen keyboard" "${KEYBOARDVER}" \
 false "SCREEN" "Useful tool for terminal management." "${SCREENVER}" \
 false "SPEEDTEST" "Run internet speedtests from the command line." "${SPEEDTESTVER}" \
 false "ELECTRUM" "Electrum BTC Wallet." "" \
 false "RASPAP" "Turn your Pi 3 into an Access Point (Hotspot)." "" \
 false "CONKY" "Conky system monitor" "${CONKYVER}" \
 false "LIBREOFFICE" "LibreOffice Suite" "${LIBREVER}" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  otherArray=(GEDIT VIM FILEZILLA MATCHBOX_KEYBOARD SCREEN SPEEDTEST ELECTRUM CONKY LIBREOFFICE)

	for i in "${otherArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi


if grep -Fxq "CONKY" /tmp/results; then
    CONKYSEL=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
--title "Conky Configuration" \
--text "Select the Conky Configuration you would like to install."  \
--list --wrap-width=150 --wrap-cols=3 \
--column="Tick" --column="Name" --column="Description" \
 false "LARGE" "Designed for 1920x1080 screens." \
 true "STANDARD" "Designed for 1280x720 screens." \
 false "MINIMAL" "Provides AmCON and TNC Status for small screen." \
 ${r} ${c})

fi

}


#### Security tools menu ###

function security_installer() {

	#Set user install directory For Security Tools.
	SecDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Packet Security Installer / Updater" \
	--text "Select your desired installation folder for Security Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Security" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${SecDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Security Tools Directory" \
	      --text "You have selected $HOME/Security as your installation directory" ${r} ${c} ||
	      { security_installer; return; }

	        SECDIR=$HOME/Security
	        if [[ ! -d $SECDIR ]]; then
	        mkdir -p $SECDIR
	        fi
	        ;;
	    Custom)
	        SECDIR=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="" --field="MDIR:MDIR" $HOME \
	        --title "Security Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { security_installer; return; }
	        if [[ ! -d $SECDIR ]]; then
	        mkdir -p $SECDIR
	        fi
	        ;;
	    esac


yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Security Programs" \
--text "Select the security programs you would like to install.  Wireless modifications will not be selected with 'Install All'"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "FAIL2BAN" "Intrusion Prevention Software. Recommended" "${FAIL2BANVER}" \
 false "KEEPASS" "KepassXC password manager" "${KEEPASSXCVER}" \
 false "ICEWEASEL" "Firefox secure browser" "${ICEWEASELVER}" \
 false "PTE" "Paranoia Tools Text Encryption.  Encrypt/Decrypt messages and perform Steganography" "${PTEVER}" \
 false "PFE" "Paranoia Tools File Encrypter.  Encrypt/Decrypt files" "${PFEVER}" \
 false "GTKHASH" "GTK based file hash tool" "${GTKHASHVER}" \
 false "GPA" "GNU Privacy Assist Key Manager" "${GPAVER}" \
 false "VERACRYPT" "Disk/File Encryption. $NEWVC" "" \
 false "BITMESSAGE" "Bitmessage secure decentralized email" "" \
 false "HARDEN_SSH" "Change folder permissions and provide SSH tips document." "" \
 false "WIFI-DISABLE" "Disable WiFi -- Raspberry Pi 3 ONLY" "" \
 false "BT-DISABLE" "Disable Bluetooth -- Raspberry Pi 3 ONLY" "" \
 false "WIFI-ENABLE" "Re-Enable WIFI -- Raspberry Pi 3 ONLY" "" \
 false "BT-ENABLE" "Re-Enable WIFI -- Raspberry Pi 3 ONLY" "" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  securityArray=(FAIL2BAN KEEPASS ICEWEASEL PTE PFE VERACRYPT BITMESSAGE HARDEN_SSH GTKHASH GPA)

	for i in "${securityArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}





################################################################################
################################################################################
################################################################################


#Lets give a quick primer on how to use this.
function primer_menu() {

echo "This program will assist you in installing or updating radio tools on your \
machine.

If you previously installed FLDIGI on this machine using a package \
manager, please quit the installer, remove FLDIGI, and restart the program.

This program is designed to build FLDIGI from source to ensure you have the newest version \
possible.

Super User (sudo) priveledges are required to perform the apt-get \
commands.

You will be asked for your password in the terminal to perform some \
tasks but you should not run the entire program as Root. " | yad \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--center \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c} ||
{ yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }

}

function notify_sources_menu() {
echo "You will need to ensure Source Code Repositories are enabled prior to \
attempting builds of HAMLIB or the FLDIGI Suite of programs.

Select 'Enable Source Code' for the installer to assist you.

Select 'Already Enabled' if you have previously completed this step." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Already Enabled:0" \
--button="Enable Source Code:2" \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c}
rc=$?

if [[ $rc == 1 ]]; then
{ yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }

elif [[ $rc == 2 ]]; then
{ enable_source_repositories; return; }

elif [[ $rc == 0 ]]; then
yad --center \
--image=$DIR/images/corps-icon.png \
--title "Source Repositories previously enabled" \
--text "You have confirmed you have enabled source repositories on your system." ${r} ${c} ||
{ yad --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }
fi
}




function enable_source_repositories() {

if [[ ${PLAT} = "Raspbian" ]]; then
	if [[ ${OSCN} = "stretch" ]]; then
sudo echo 'deb-src http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi' | \
sudo tee -a /etc/apt/sources.list && sudo apt-get -y update && \
yad \
--image=$DIR/images/corps-icon.png \
--center --button="gtk-ok:0" \
--title "Source Code Repositories Enabled" \
--text "Successfully enabled source code repositories" ${r} ${c}

	elif [[ ${OSCN} = "buster" ]]; then

sudo echo 'deb-src http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi' | \
sudo tee -a /etc/apt/sources.list && sudo apt-get -y update && \
yad \
--image=$DIR/images/corps-icon.png \
--center --button="gtk-ok:0" \
--title "Source Code Repositories Enabled" \
--text "Successfully enabled source code repositories" ${r} ${c}	

	fi
elif [[ ${PLAT} = "Ubuntu" ]]; then

software-properties-gtk

elif [[ ${PLAT} = "LinuxMint" ]]; then

software-sources

else

yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Not a Supported OS" --text "Please manually enable software sources before continuing." ${r} ${c}
exit 1
fi

rc=$?

if [[ $rc -eq 0 ]]; then
{ notify_sources_menu; return; }

elif [[ $rc -eq 1 ]]; then
if [[ ${PLAT} = "Raspbian" ]]; then
echo "This installer failed to modify your /etc/apt/sources.list/n/nPlease manually \
uncomment the deb-src line by removing the leading # before running this installer" | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-ok:0" \
--title "Software Sources Modification Failed" \
${r} ${c}
exit 1

else
echo "This installer failed to open your Sofware Sources menu.\n\nYou can do this \
by pressing the Super-Key (Windows Key) and searching for 'Software Sources' in Mint or \
'Software and Updates,' in Ubuntu.\n\nCheck to ensure Source Code is enabled prior to \
running this installer." | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-ok:0" \
--title "Software Sources Modification Failed" \
 ${r} ${c}
{ notify_sources_menu; return; }
fi

fi
}

################################################################################
###################  Perform system updates to begin  ##########################
################################################################################

function system_update_menu() {

echo "It is recommended that you perform a system update before continuing.

If you have not performed a sudo apt-get update and sudo apt-get upgrade today \
select 'Update' to perform it now.

Otherwise, select 'Skip' to continue without checking for updates.

Would you like to perform a repository update at this time?

**RECOMMENDED**" | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-close:1" --button="Skip:2" --button="Update:0" \
--title "System Update Recommended" \
${r} ${c}
rc=$?
  if [[ $rc == 0 ]]; then
    sudo apt-get -y update && sudo apt-get -y upgrade
	elif [[ $rc == 2 ]]; then
		echo "Skipping update check this time"
	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1
  fi



# Install git now.
if hash git 2>/dev/null; then
    echo "Git is already installed"
        else
            sudo apt-get -y install git
fi

}

################################################################################
##############################   Swap Size   ###################################
################################################################################
function swap_size_check() {

if [[ ${PLAT} = "Raspbian" ]]; then

SWAPFILE=/etc/dphys-swapfile

CURRENTSWAPSIZE=$(cat $SWAPFILE | grep CONF_SWAPSIZE= | awk -F "=" '{print $2}')

if [[ $CURRENTSWAPSIZE -lt 2000 ]]; then

    echo "Some users have found that increasing the swap size may help prevent crashing during compiling of FLDIGI.

Your current swapfile size is $CURRENTSWAPSIZE.

2000 is recommended.

A reboot will be required before continuing." | yad \
 --image=$DIR/images/corps-icon.png \
 --center \
 --text-info \
 --wrap \
 --button="Quit:1" \
 --button="No:2" \
 --button="Increase:0" \
 --title "Swap Size." \
 ${r} ${c}
rc=$?

 if [[ $rc == 2 ]]; then
	echo "You selected to not increase your swap file size.  If your system freezes up on compiling FLDIGI, please try increasing the swap file." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Continue:0" \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c}
	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1
    elif [[ $rc == 0 ]]; then
        sudo sed -i s/CONF_SWAPSIZE=${CURRENTSWAPSIZE}/CONF_SWAPSIZE=2000/g $SWAPFILE && \
            yad --image=$DIR/images/corps-icon.png \
            --center \
	    --title="Swap Size" \
            --text="Your machine will reboot in 10 seconds" \
            --timeout=10 \
            --timeout-indicator=top \
            --button="Cancel:1" \
            --button="Reboot Now:0" \
            ${r} ${c}
             rc=$?
             if [[ $rc == 1 ]]; then
                 exit 1
             else
                 sudo reboot
             fi

  fi
fi


fi
}

################################################################################
##############################  Master Menu  ###################################
################################################################################

function main_menu() {

yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Categories" \
--text "Select the categories of programs you would like to install"  \
--list --wrap-width=200 --wrap-cols=3 \
--column="Tick" --column="Category" --column="Description" \
 false "RADIO" "FLDIGI suite, ARDOP/ARIM, CHIRP, AmRRON Forms, etc." \
 false "PACKET" "Minicom, GTKTerm, Direwolf, etc" \
 false "SDR" "Nothing in here yet.  Stay tuned..." \
 false "SECURITY" "Veracrypt, Fail2ban, KeepassXC, Bitmessage, disable wireless etc." \
 false "OTHER" "Electrum, Hotspot, touch keyboard, gedit,etc" \
 false "ICONS" "Creates Desktop Icons for installed programs" \
 ${r} ${c} >>/tmp/mastermenu



CHOICES=$(cat /tmp/mastermenu)
	while read mastermenu
	do
		case $mastermenu in
			RADIO) echo "Selecting Radio Tools menu"
				radio_installer
				;;
			PACKET) echo "Selecting Packet Tools menu"
				packet_installer
				;;
			SECURITY) echo "selecting Security Tools menu"
				security_installer
				;;
			SDR) echo "selecting SDR Tools menu"
				sdr_installer
				;;
			OTHER) echo "selecting Other Tools menu"
				other_installer
				;;
			ICONS) echo "ICONS" >> /tmp/results
		esac
	done < /tmp/mastermenu

}



################################################################################
#########################  Execute the selections  #############################
################################################################################

function selection_results() {

if [[ -s /tmp/results ]]; then
cat /tmp/results | yad --text-info --center \
--image=$DIR/images/corps-icon.png \
--button="Quit:1" \
--button="Begin:0" \
--title "Confirm Update Selection" \
--text "You have chosen to install / update the following:" ${r} ${c} || exit 1
 else
    yad --center \
	--image=$DIR/images/donate.png \
	--button="gtk-ok:0" \
	--title "Goodbye" \
	--text "You did not make any selections.\n\nThe installer will now exit.\n\nThank you." ${r} ${c}
    exit 1
 fi


 while read results
 do
	 case $results in

  	HAMLIB) echo "Updating HAMLIB"
		hamlib_install
   		;;
  	FLDIGI) echo "Installing / Updating FLDIGI"
		fldigi_install
   		;;
  	FLMSG) echo "Updating FLMSG"
  		flmsg_install
   		;;
  	FLAMP) echo "Updating FLAMP"
  		flamp_install
   		;;
	FLWRAP) echo "Updating FLWRAP"
  		flwrap_install
   		;;
	FLRIG) echo "Updating FLRIG"
  		flrig_install
   		;;
  	ARDOP) echo "Updating ARDOP"
   		ardop_install
   		;;
	ARDOPGUI) echo "Updating ARDOP_GUI"
   		ardop_gui_install
   		;;
  	ARIM) echo "Updating ARIM"
   		arim_install
   		;;
	GARIM) echo "Updating GARIM"
   		garim_install
   		;;
	WSJTX) echo "Installing WSJTX"
		wsjtx_install
		;;
	PULSE) echo "Installing pulseaudio suite"
		pulse_audio_suite_install
		;;
  	FORMS) echo "Retriving AmRRON Forms"
   		amrronforms_install
   		;;
  	ICONS) echo "Creating Desktop Shortcuts."
   	 	echo desktopshortcuts >> /tmp/cleanup
   		;;
  	PAT) echo "Updating PAT"
	 	pat_install
		;;
  	FINDARDOP) echo "Getting KM4ACK's find ardop tools."
   		getardoptools_install
   		;;
	VOACAP) echo "Getting VOACAP Propagation tool."
   		voacap_install
   		;;
	CHIRP) echo "Updating Chirp"
		chirp_install
		;;
	JS8CALL)
		js8call_install
		;;
	UBLOX)
		ublox_gps_setup
		echo ublox >> /tmp/cleanup
		;;
  	MINICOM) echo "Installing Minicom"
		minicom_install
		;;
	GTKTERM) echo "Installing GtkTerm"
		gtkterm_install
	  	;;
	PUTTY) echo "Installing Putty"
		putty_install
	  	;;
	DIREWOLF) echo "Installing Direwolf"
		direwolf_install
		;;
	YAAC) echo "Skipping YAAC"
		yaac_install
		;;
	PACKET_GUIDE) echo "Creating Packet Radio Guide"
		packet_guide
		;;
	GEDIT) echo "Installing Gedit"
		gedit_install
		;;
	VIM) echo "Installing VIM"
		vim_install
		;;
	FILEZILLA) echo "Installing Filezilla"
		filezilla_install
		;;
	MATCHBOX_KEYBOARD) echo "Installing On-Screen Keyboard"
		keyboard_install
	  	;;
	SCREEN) echo "Installing Screen"
		screen_install
	  	;;
	SPEEDTEST) echo "Installing Speedtest"
		speedtest_install
	  	;;
	RASPAP)
		raspap_install
		;;
	ELECTRUM) echo "Installing Electrum Bitcoin Wallet"
		electrum_install
	  	;;
	CONKY)
		conky_install
		;;
	FAIL2BAN) echo "Installing fail2ban"
		fail2ban_install
	        ;;
	KEEPASS) echo "Installing keepassxc"
		keepassxc_install
	        ;;
	ICEWEASEL) echo "Installing iceweasel"
		iceweasel_install
	        ;;
	GTKHASH) echo "Installing GTK Hash"
		gtkhash_install
		;;
	GPA) echo "Installing GNU Privacy Assist"
		gpa_install
		;;
	HARDEN_SSH) echo "Hardening SSH and creating guide at $HOME/Documents"
		harden_ssh
	        ;;
	PTE) echo "Installing Paranoia Text Encrypter"
		pte_install
		;;
	PFE) echo "Installing Paranoia File Encrypter"
		pfe_install
		;;
	BITMESSAGE) echo "Installing Bitmessage"
		bitmessage_install
	        ;;
	VERACRYPT) echo "Installing Veracrypt"
		veracrypt_install
		;;
	WIFI-DISABLE) echo "Disabling wireless hardare"
		wifi_off
	        ;;
	BT-DISABLE) echo "Disabling Bluetooth"
		bt_off
	        ;;
	WIFI-ENABLE) echo "Re-Enabling wireless hardare"
		wifi_on
	        ;;
	BT-ENABLE) echo "Re-Enabling Bluetooth"
		bt_on
	        ;;
	LIBREOFFICE) echo "Installing LibreOffice Suite"
		libreoffice_install
		;;
esac
done < /tmp/results

}


################################################################################
################################################################################
############################  Failure Notifications ############################
################################################################################
################################################################################

function failure_handling() {

FAIL=$(cat /tmp/failures) || echo "No failures recorded"


if [[ -f /tmp/failures ]]; then
yad --image=$DIR/images/corps-icon.png --center --button="Save Log:2" --button="gtk-ok:0" \
--title "Failures" \
--text "The following failures occured during installation:\n\n$FAIL" ${r} ${c} exit=$?
if [[ $? == 2 ]]; then
ERRORLOG=$HOME/AmRRON-Setup-Tool-failures
if [[ -e $ERRORLOG.log ]]; then
    i=1
    while [[ -e $ERRORLOG-$i.log ]] ; do
        let i++
    done
    ERRORLOG=$ERRORLOG-$i
fi
cp /tmp/failures $ERRORLOG.log
yad --center --button="gtk-ok:0" \
--image=$DIR/images/corps-icon.png \
--title "Failure Log Saved" \
--text "A copy of the failure log was saved at $ERRORLOG.log" ${r} ${c}
fi

fi

}

################################################################################
################################################################################
##################################  Clean up  ##################################
################################################################################
################################################################################

function cleanupyad() {
echo "${TEXT}" | yad \
--center --text-info \
--image=$DIR/images/corps-icon.png --wrap \
--title "${TITLE}" \
${r} ${c}
}

function final_cleanup() {

while read cleanup
do
    case $cleanup in

	desktopshortcuts)
	create_icons
	;;
	ardop)

TITLE="ARDOP Versions"
TEXT="There are two main version of the ARDOP TNC.  This installer \
used install version 1 on your system.  Version 1 has increased \
stability and is the default TNC used for AmRRON ARDOP/ARIM \
communications at this time."
    cleanupyad

	;;

	asoundrc)

TITLE="ARDOP configuration"
TEXT="A configuration file is required for your sound device to \
work with the ARDOP TNC.

This file is located at $HOME/.asoundrc

You may need to substitute the appropriate hardware location for your \
device.

You can determine your soundcard location by running the \
command

aplay -l

This will show your attached sound devices and their card numbers.  \
Please edit your $HOME/.asoundrc file appropriately if your card is not 1."
   cleanupyad

	;;

    ardopguifail)
TITLE="ARDOP_GUI Not Supported"
TEXT="ARDOP_GUI requires qt5 in order to operate.

At this time, I have not determined the full set of dependencies in order to run \
ARDOP_GUI on a 64bit OS prior to Mint 19+ and Ubuntu 18.04+"
  cleanupyad
        ;;

 	ardoptools)

TITLE="KM4ACK Find Ardop"
TEXT="KM4ACK's script has been installed.  The commands are getardoplist \
and findardop.

This requires PAT to be installed and configured in order to work properly.

If you'd like to automate the station list update, perform the following from terminal:

crontab -e

Select your favorite editor, nano is easy.

Add the following to the bottom:

30 23 * * * /usr/local/bin/getardoplist

Save and exit

The above example will update your list every night at 2330 system time."
  cleanupyad

  ;;

	wsjtxunsupported)
TITLE="WSJTX Not Supported"
TEXT="WSJTX version 2.0 and greater do not have an installation package compatible \
with your OS.  Ubuntu 18.04 and Mint 19 or greater are recommended.  You can attempt \
to compile from source.  Future versions of this installer may support compiling."
  cleanupyad

	;;

    js8callnotice)
echo "This installer searched for, downloaded and installed JS8Call in order to make the initial setup of your machine easier and provide an easy way to locate and apply the next updates...

The author's intent however, is for all users of his software join the JS8Call mailing group.  This allows for the author to communicate changes the users, gauge the level of interest in his work, and for users to share bugs with the community.  Please take a moment to follow this link, register an account if needed. and join the JS8Call group.

https://groups.io/g/js8call" | yad --center --text-info --wrap --image=$DIR/images/corps-icon.png --show-uri --title "JS8Call Installed" ${r} ${c}

        ;;

	ublox)
TITLE="UBLOX GPS Time"
TEXT="You selected to setup your system to work with the Ublox GPS for system time.

Settings for this are contained in /etc/default/gpsd and /etc/chrony/chrony.conf.

If you are using a different gps, you may need to modify the settings in these files."
cleanupyad
	;;

	wirelessoff)

TITLE="Wireless Disable"
TEXT="You have selected to disable WIFI / Bluetooth from this system.  \
Changes will take effect upon reboot.

An information text file has been created in $HOME/Documents for details."
  cleanupyad

	;;

	wirelessnotoff)

TITLE="Wireless Enable Failure"
TEXT="You have selected to enable WIFI / Bluetooth from this system but \
no entry disabling these was found."
cleanupyad

	;;

  	wirelesserror)

TITLE="Wireless Modification Failure"
TEXT="You have selected to modify WIFI / Bluetooth from this system.

This function is only applicable to Raspberry Pi 3 but the installer did not \
recognize your system as a Raspberry Pi"
cleanupyad

	;;

  	noelectrum)

TITLE="Electrum Unavailable"
TEXT="You have selected to install Electrum on a Raspberry Pi or a system \
that does not natively support Python 3.6.1 or greater.

At this time, the required Python3 modules are not compatible with your system \
or will need to be self satisfied."
cleanupyad

	;;

#	vc)

#    if [[ ${PLAT} = "Raspbian" ]]; then
#
#TITLE="Veracrypt Raspbian"
#TEXT="Version 1.0f-1 has been downloaded.
#
#You will have to complete installation once this installer exits.
#
#You can do this by navigating to $SECDIR and executing \
#${VCFILE_name}."
#cleanupyad
#
# else

#TITLE="Veracrypt Install"
#TEXT="Version $NEWVC has been downloaded and extracted.
#
#You will have to complete installation once this installer exits.
#
#You can do this by navigating to the $SECDIR and executing \
#veracrypt-$NEWVC-setup-gui-$VCSYS"
#    cleanupyad
#
# fi

#  	;;

	raspapsetup)

TITLE="RaspAP Setup."
TEXT="A seperate installer for the RaspAP has been downloaded to your desktop.

Additionally, further information can be found in \
$HOME/Documents/RaspAP-Hotspot-Setup.txt

Run the script from the Desktop after rebooting from the current installations."
cleanupyad

  	;;

	noraspapsetup)

TITLE="RaspAP Setup."
TEXT="You have selected the RaspAP installation but the installer did not detect \
your system as Raspbian.

Installation skipped."
    cleanupyad

    	;;

	js8call_os)

TITLE="JS8Call Unsupported Operating System."
TEXT="JS8Call does not yet support Ubuntu 19.10."
    cleanupyad

    	;;

voacap_os)

TITLE="VOACAPL pythonprop GUI Unsupported Operating System."
TEXT="VOACAPL pythonprop GUI does not yet support Ubuntu 19.10."
    cleanupyad

    	;;

    pybitmessagesetup)
if [[ ${PLAT} = "Raspbian" ]]; then
echo "Add @/usr/local/bin/pybitmessage to the file /etc/xdg/lxsession/LXDE-pi/autostart \
to enable autostart of pybitmessage on the Pi. Would you like me to attempt to do this now?" | yad \
--text-info --image=$DIR/images/corps-icon.png --center --wrap \
--button="No:0" --button="Enable Autostart:2" \
--title "PyBitmessage Autostart Setup." \
${r} ${c}
rc=$?
if [[ $rc -eq 2 ]]; then
	echo "@/usr/local/bin/pybitmessage" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart ||
                      { ERRCODE="Failed to setup pybitmessage autostart"; errors; }
fi
else
TITLE="Pybitmessage Autostart Setup."
TEXT="If you would like to auto start pybitmessage on boot, please use your distribution's Startup \
Applications setup and add pybitmessage to the list."
    cleanupyad
fi
	;;




	conkysetup)
echo "Conky has been intalled and a configuration file created at $HOME/.conkyrc.  \
You can run conky by typing in a terminal:
conky &
Add @/usr/bin/conky to the \
file /etc/xdg/lxsession/LXDE-pi/autostart on raspbian or in linux create a desktop file \
in $HOME/.config/autostart/ to enable autostart of conky.
Would you like me to attempt to do this now?" | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap --center --button="No:0" --button="Enable Autostart:2" \
--title "Conky Autostart Setup." ${r} ${c}
rc=$?
if [[ $rc -eq 2 ]]; then

    if [[ ${PLAT} = "Raspbian" ]]; then
	echo "@/usr/bin/conky -p 8 -d" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart ||
                      { ERRCODE="Failed to setup conky autostart"; errors; }

    else

mkdir -p  ~/.config/autostart
echo "[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Conky
Comment=System Monitor
Exec=/usr/bin/conky -p 8 -d
StartupNotify=false
Terminal=false
Hidden=false" >  $HOME/.config/autostart/conky.desktop ||
 { ERRCODE="Failed to setup conky autostart"; errors; }

fi
fi
	;;

esac
done </tmp/cleanup

echo "All selected updates have been completed." | yad --image=$DIR/images/donate.png \
--text-info --center --wrap --button="gtk-ok:0" \
--title "Complete" ${r} ${c}

echo "It is recommended to restart your system before continuing.

Would you like to restart now?" | yad --center \
--text-info --wrap \
--image=$DIR/images/corps-icon.png \
--title "Restart" --button="No:1" --button="Restart:2" ${r} ${c}
rc=$?
  if [[ $rc -eq 2 ]]; then
	sudo shutdown -r now
  elif [[ $rc -eq 1 ]]; then
echo "Good luck and 73's" | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap \
--center --button="gtk-ok:0" \
--title "Thank you." ${r} ${c}
exit 1
  else
	echo "You closed the window without making a selection.  Installer exiting."

exit 1
  fi

}



################################################################################
##########################  Troubleshooting Area  ##############################
################################################################################






################################################################################
########################  Actually run this thing  #############################
################################################################################

set_screen_size
check_installed_programs
query_updates
primer_menu
#notify_sources_menu
system_update_menu
swap_size_check
main_menu
selection_results
failure_handling
final_cleanup

################################################################################
################################################################################
#################################  TB-14 2019  #################################
################################################################################
################################################################################
