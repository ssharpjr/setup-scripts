#!/bin/bash
 ################################################################################
 ################################################################################
 #################  AmRRON PAT Setup and Alias Manager v0.5.2  ##################
 #################              Created by TB-14               ##################
 #################                20 Dec 2019                  ##################
 ################################################################################
 ################################################################################


function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"

}


# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"


function dep_check() {



if ! hash jq 2>/dev/null; then
    sudo apt install -y jq
fi

if ! hash yad 2>/dev/null; then
    sudo apt install -y yad
fi

if ! hash pat 2>/dev/null; then
    echo "Pat is not installed.  Please install pat and try again.

    https://getpat.io" | yad --center --text-info --wrap --image=$DIR/images/corps-icon.png --show-uri --button="Exit:1" --title "Install PAT" ${r} ${c} || exit 1
fi

if ! hash getardoplist 2>/dev/null; then
    wget -P /tmp/ -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
    { echo "Failed to download getardop script"; }

    chmod +x /tmp/getardoplist && sudo mv /tmp/getardoplist /usr/local/bin/ ||
        { echo "Failed to add getardoplist to /usr/local/bin"; }

fi

if [[ ! -f $HOME/Documents/grid-map.pdf ]]; then
    wget -O $HOME/Documents/grid-map.pdf http://www.icomamerica.com/en/amateur/amateurtools/2013_GridSquareMap.pdf
fi

}

#Lets grab the AmRRON Banner.
#wget -N http://s9756.storage.proboards.com/5619756/a/KWTGc2YscxSclVEeB5jD.png -O /tmp/banner.png


#create a finish trap
function finish {
  if [[ -f /tmp/patconfig.json ]]; then
rm /tmp/patconfig.json
fi

if [[ -f /tmp/stationlist ]]; then
rm /tmp/stationlist
fi

if [[ -f /tmp/aliaschoices ]]; then
rm /tmp/aliaschoices
fi

if [[ -f /tmp/currentaliases ]]; then
rm /tmp/currentaliases
fi

if [[ -f /tmp/removealiases ]]; then
rm /tmp/removealiases
fi

if [[ -f /tmp/banner.png ]]; then
rm /tmp/banner.png
fi

}
trap finish EXIT


##Some variables needed
PATCONFIG="$HOME/.wl2k/config.json"
TMPFILE="/tmp/patconfig.json"


function skip_setup() {

## Make backup of current config.
cp $PATCONFIG $PATCONFIG.$(date +%h-%d-%Y@%H%M).bak


if ( [[ "$(jq .mycall $PATCONFIG | tr -d \")" != "" ]] && [[ "$(jq .locator $PATCONFIG | tr -d \")" != "" ]] ); then
        findalias_menu
    else
        main_menu
    fi
}


function main_menu() {
if [[ ! -f $PATCONFIG ]]; then
    pat configure | sleep 2 | kill $(pidof pat) ||
        { echo "No config.json file exists.  Please run 'pat configure' from a terminal and then CTRL-X to close the file.  This will generate a blank file which this tool will assist you with."; exit 1; }
    fi

CURRENTCALL=$(jq ".mycall" $PATCONFIG | tr -d \")
CURRENTPASS=$(jq ".secure_login_password" $PATCONFIG | tr -d \")
CURRENTLOCATOR=$(jq ".locator" $PATCONFIG | tr -d \")
CURRENTHTTP=$(jq ".http_addr" $PATCONFIG | tr -d \")
CURRENTRIG=$(jq ".ardop.rig" $PATCONFIG | tr -d \")

PATSETUP=$(yad --center --wrap --title="PAT Setup and Alias Manager" \
--text="--Please ensure you have a valid winlink account and password established.\n--The raspberry pi sometimes has problems understanding 'localhost.' Use 127.0.0.1 Using 0.0.0.0 will allow you to access the PAT UI from your local network.\n--Point your browser to the  local ip and port.  Example: 192.168.1.15:8080" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Enter your callsign" "$CURRENTCALL" \
--field="Enter your winlink password:H" "$CURRENTPASS" \
--field="Enter your station grid square, ex. DN23" "$CURRENTLOCATOR" \
--field="Select or enter your local address and port:"CBE "$CURRENTHTTP,localhost:8080,0.0.0.0:8080,127.0.0.1:8080" \
--field="Radio Model. i.e IC-7300" "$CURRENTRIG" \
--field="Add/Change Rig Definition to PAT Config?:CHK" "False" \
--field="Disable Rig Control for PAT?:CHK" "False" \
${c} ${r}) || exit 1

NEWCALL=$(echo "$PATSETUP" | awk -F "," '{print $1}')
NEWPASS=$(echo "$PATSETUP" | awk -F "," '{print $2}')
NEWLOCATOR=$(echo "$PATSETUP" | awk -F "," '{print $3}')
NEWHTTP=$(echo "$PATSETUP" | awk -F "," '{print $4}')
NEWRIG=$(echo "$PATSETUP" | awk -F "," '{print $5}')
CHANGEDEF=$(echo "$PATSETUP" | awk -F "," '{print $6}')
DISABLEDEF=$(echo "$PATSETUP" | awk -F "," '{print $7}')


if [[ $CURRENTCALL != $NEWCALL ]]; then
    jq '.mycall = '\"$NEWCALL\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTPASS != $NEWPASS ]]; then
    jq '.secure_login_password = '\"${NEWPASS}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTLOCATOR != $NEWLOCATOR ]]; then
    jq '.locator = '\"${NEWLOCATOR}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTHTTP != $NEWHTTP ]]; then
    jq '.http_addr = '\"${NEWHTTP}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CHANGEDEF == "TRUE" ]]; then

    if [[ $NEWRIG != "" ]]; then

   jq '.hamlib_rigs = {'\"$NEWRIG\"': {"address": "127.0.0.1:4532", "network": "tcp"}}' $PATCONFIG | jq '.ardop.rig = '\"${NEWRIG}\"'' | jq '.ardop.ptt_ctrl = true' > $TMPFILE
    cp $TMPFILE $PATCONFIG

    else

	echo "You must enter a Rig name if selecting to change the rig definition before continuing." | yad --center --text-info --wrap --title=TriMode --button="Start Over:1" ${c} ${r} ||
	{ main_menu; return; }


    fi

fi

 if [[ $DISABLEDEF == "TRUE" ]]; then

     if [[ $CHANGEDEF == "TRUE" ]]; then

    echo "You may select EITHER Add/Change or Remove as an option, not both." | yad \
        --center \
        --text-info \
        --wrap -\
        -title=TriMode \
        --button="Start Over:1" \
        ${c} ${r} ||
     { main_menu; return; }

    else

    jq '.ardop.ptt_ctrl = false' $PATCONFIG | jq '.ardop.rig = '\"\"'' | jq 'del(.hamlib_rigs.'\"$CURRENTRIG\"')' > $TMPFILE
     cp $TMPFILE $PATCONFIG
 fi

fi

findalias_menu


}

function findalias_menu() {

LISTLOC="$HOME/Documents/ardop-list"

    if [[ -f $LISTLOC/ardoplist.txt ]]; then
    LASTUPDATE=$(cat $HOME/Documents/ardop-list/ardoplist.txt | head -n 1)
    else
       getardoplist | yad --center --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 --title "Downloading ARDOP Station lists" --text "Please wait while I download the current list of ARDOP stations..." ${r} ${c}
    fi
VIEWER="evince -w"
GRIDMAP="$HOME/Documents/grid-map.pdf"
BANDS=$(echo 20M,30M,40M,80M)
GRIDS=$(echo CN,DN,EN,FN,CM,DM,EM,FM,DL,EL,FL)
if [[ $(jq .ardop.rig $PATCONFIG | tr -d \") == "" ]]; then
    FORMAT=$(echo "Manual Tuning,Rigctl Tuning")
else
    FORMAT=$(echo "Rigctl Tuning,Manual Tuning")
fi

FINDALIAS=$(yad --center --wrap --show-uri \
    --title="PAT Setup and Alias Manager" \
    --text="$LASTUPDATE\n\nSelect PAT Setup to open user and radio config.\nSelect MANAGE to view or remove current aliases.\nSelect Update to Re-Download the list of stations\nThis may take a couple minutes.\nUpdates will be visable from the PAT UI after restarting the pat http server." \
    --image=$DIR/images/corps-icon.png \
    --button="PAT Setup:3" \
    --button="MANAGE:4" \
    --button="UPDATE:2" \
    --button="Exit:1" \
    --button="Find Stations:0" \
    --form --separator="," --item-separator="," \
    --field="Select desired band:CB" $BANDS \
    --field="Enter Target Grid Square:CBE" "$GRIDS" \
    --field="Click to View Grid Square Map:BTN" "$VIEWER $GRIDMAP" \
    --field="Alias Format.:CB" "$FORMAT" \
${c} ${r})
rc=$?
if [[ $rc -eq 2 ]]; then
    { getardoplist | yad --center --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 --title "Downloading ARDOP Station lists" --text "Please wait while I download the current list of ARDOP stations..." ${r} ${c}; findalias_menu; return; }
elif [[ $rc -eq 1 ]]; then
    exit 1
elif [[ $rc -eq 3 ]]; then
    { main_menu; return; }
elif [[ $rc -eq 4 ]]; then
    { manage_aliases; return; }
fi

BAND=$(echo $FINDALIAS | awk -F "," '{print $1}' | tr [:upper:] [:lower:])
GRID=$(echo $FINDALIAS | awk -F "," '{print $2}')
TUNEMODE=$(echo $FINDALIAS | awk -F "," '{print $4}' | awk '{print $1}')

cat $LISTLOC/${BAND}ardoplist.txt | awk '$2 ~/'${GRID}'/{print $1,$2,$3,$4,$7,$11}' | sed 's/^/false /' | tr ' ' '\012' > /tmp/stationlist


cat /tmp/stationlist | yad --image=$DIR/images/corps-icon.png --center \
    --checklist \
    --separator="," \
    --title "ARDOP Stations" \
    --text "Select the stations you would like to add to your PAT alias list" \
    --list \
    --button="Exit:1" \
    --button="Change Band:2" \
    --button="Add Aliases:0" \
    --column="Add to Aliases?" \
    --column="Callsign" \
    --column="Grid-Square" \
    --column="Distance" \
    --column="Direction" \
    --column="Dial Freq" \
    --column="Alias" \
    ${r} ${c} > /tmp/aliaschoices
rc=$?

if [[ $rc -eq 0 ]]; then

while read file;
do
   CALLSIGN=$(echo $file |  awk -F "," '{print $2}')
   DISTANCE=$(echo $file |  awk -F "," '{print $4}')
   FREQUENCY=$(echo $file | awk -F "," '{print $6}')
   if [[ "$TUNEMODE" == "Rigctl" ]]; then
   ALIAS=$(echo $file |  awk -F "," '{print $7}')
   else
   ALIAS=$(echo $file |  awk -F "," '{print $7}' | awk -F "?" '{print $1}')
   fi
   NAME="$CALLSIGN--$DISTANCE--$FREQUENCY"

jq '.connect_aliases += {'\""$NAME"\"' : '\"$ALIAS\"'}' $PATCONFIG > $TMPFILE
cp $TMPFILE $PATCONFIG

done < /tmp/aliaschoices

{ findalias_menu; return; }

elif [[ $rc -eq 2 ]]; then
    { findalias_menu; return; }

elif [[ $rc -eq 1 ]]; then
    exit 1

fi

}

function manage_aliases() {

jq .connect_aliases $PATCONFIG | sed '/^}/d' | sed '/^{/d' | sed 's/^ *//g' | sed 's/^/false /' | tr ' ' '\012' > /tmp/currentaliases



CURRENT=$(cat /tmp/currentaliases | yad --image=$DIR/images/corps-icon.png --center --checklist --separator="," --title "Current Aliases" --text "Select any aliases you would like to remove" --list --print-column=2 --button="Exit:1" --button="Return:2" --button="Apply:0" --column="Saved Alias" --column="Name-Dist-Freq" --column="Alias"  ${r} ${c} > /tmp/removealiases)

rc=$?

if [[ $rc -eq 0 ]]; then

while read file;
do
    TOREMOVE=$(echo $file | awk -F ":" '{print $1}')

   jq 'del(.connect_aliases.'$TOREMOVE')' $PATCONFIG > $TMPFILE

   cp $TMPFILE $PATCONFIG

done < /tmp/removealiases

{ findalias_menu; return; }


elif [[ $rc -eq 2 ]]; then

    findalias_menu
elif [[ $rc -eq 1 ]]; then
    exit 1
fi



}




set_screen_size
dep_check
skip_setup
